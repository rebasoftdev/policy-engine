package net.rebasoft.data;

import org.apache.log4j.Logger;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Abstract repository class to provide basic Read, Update, Delete functionality using SQL (JDBC) with added hierarchical (tree) operations.
 * This also extends AbstractRepositoryJson so provides methods for accessing the repository using JSON to retrieve/update/deleteById
 * the data.
 * <p>
 * Created by chriswhiteley on 14/04/2017.
 *
 * @param <T>  The entity type.
 * @param <ID> The primary key type.
 *             <p>
 *             Created by chriswhiteley on 09/03/2017.
 */
public abstract class AbstractCachedSQLTreeRepository<T, ID extends Serializable & Comparable<ID>> extends AbstractCachedSQLRepository<T, ID> implements RepositoryTree<T, ID> {
    private static final Logger log = Logger.getLogger("AbstractCachedSQLTreeRepository");
    private final boolean hierachyInSameTable = tableName().equalsIgnoreCase(hierarchicalTableName());
    private final String REMOVE_CHILD_SAME_TABLE_SQL = "UPDATE " + tableName() + " SET " + parentColumnName() + " = NULL WHERE " + idColumnName() + " =?";
    private final String REMOVE_CHILD_SEPARATE_TABLE_SQL = "DELETE FROM " + hierarchicalTableName() + " WHERE " + parentColumnName() + " =? AND " + childColumnName() + " =?";
    private final String REMOVE_ALL_CHILDREN_SAME_TABLE_SQL = "UPDATE " + tableName() + " SET " + parentColumnName() + " = NULL";
    private final String REMOVE_ALL_CHILDREN_SEPARATE_TABLE_SQL = "DELETE FROM " + hierarchicalTableName();
    private final String ADD_CHILD_SAME_TABLE_SQL = "UPDATE " + tableName() + " SET " + parentColumnName() + " = ? WHERE " + idColumnName() + " =?";
    private final String ADD_CHILD_SEPARATE_TABLE_SQL = "INSERT INTO " + hierarchicalTableName() + " (" + parentColumnName() + ", " + childColumnName() + ") VALUES (?, ?)";
    private String selectAllHierarchicalSql;

    protected abstract String childColumnName();

    protected abstract String parentColumnName();

    public AbstractCachedSQLTreeRepository(Class<T> typeParameterClass, DataSource dataSource, boolean idSequenceGenerated) {
        super(typeParameterClass, dataSource, idSequenceGenerated);
    }

    private String getSelectAllHierachicalSql() {
        if (selectAllHierarchicalSql == null) {
            selectAllHierarchicalSql = "SELECT " + parentColumnName() + ", " + childColumnName() + " FROM " + hierarchicalTableName();
        }

        return selectAllHierarchicalSql;
    }


    protected void populateCache() {
        synchronized (cache) {
            super.populateCache();

            // now populate the hierarchy in the cache
            try {
                try (Connection conn = dataSource.getConnection();
                     Statement statement = conn.createStatement()
                ) {
                    ResultSet rs = statement.executeQuery(getSelectAllHierachicalSql());
                    while (rs.next()) {
                        ID parentId = getParentIdFromResultSet(rs);
                        ID childId = getChildIdFromResultSet(rs);
                        updateCacheHierarchy(parentId, childId);
                    }
                }
            } catch (SQLException e) {
                throw new RepositoryException(String.format("Error populating cache hierarchy using query %s", getSelectAllHierachicalSql()), e);
            }
        }
    }

    private void updateCacheHierarchy(ID parentId, ID childId) {
        TreeNode<T> parentNode = getCacheEntry(parentId).getNode();
        TreeNode<T> childNode = getCacheEntry(childId).getNode();
        parentNode.addChildNode(childNode);
    }

    protected abstract ID getChildIdFromResultSet(ResultSet rs) throws SQLException;

    protected abstract ID getParentIdFromResultSet(ResultSet rs) throws SQLException;

    <S extends T> void updateCache(S entity) {
        synchronized (cache) {

            TreeNodeCacheEntry<T> cacheEntry = getCacheEntry(getId(entity));
            T oldEntity = (cacheEntry == null) ? null : cacheEntry.get();
            S updatedEntity = (S) preUpdateCache(oldEntity, entity);

            if (cacheEntry == null) {
                cacheEntry = new TreeNodeCacheEntry(updatedEntity);
                cache.put(getId(entity), cacheEntry);
            } else {
                cacheEntry.set(updatedEntity);
            }

            if (entity instanceof EntityWithEmcId) {
                EntityWithEmcId entityWithEmcId = (EntityWithEmcId) entity;

                if (entityWithEmcId.getEmcId() > 0) {
                    emcIdToIdCache.put(entityWithEmcId.getEmcId(), getId(entity));
                }
            }

            log.debug("Entity updated in cache: " + entity);
        }
    }


    /**
     * This should return the name of the table that contains the parent/child hierarchy information.  Two hierarchy structures are
     * supported:
     * <p>
     * 1. If this returns the same name as tableName() method then it is assumed that each record in the table has a parent key
     * column pointing to the parent in the same table.  For a root node this would be null, for a child node this would point to the
     * record that is its parent.  To add a child record the child record would be located and its parent key updated to point to
     * the parent it belongs to.  To remove a child from a parent then on the child record the parent key would be set to null.
     * <p>
     * 2. If this returns a different name to tableName() method then it is assumed that each record in this separate hieratchy table contains a parent id
     * and a child id.  To add a child a new record is created with the new parent id/child id combination.  To remove a child from a parent
     * then the record with the parent id/ child id combination is deleted.
     *
     * @return
     */
    protected abstract String hierarchicalTableName();


    protected abstract void setHierachicalParameterValues(PreparedStatement ps, T parent, T child, SqlOperation operation) throws SQLException;

    @Override
    public <S extends T> S delete(T entity) {
        synchronized (cache) {
            if (entity == null) return null;
            TreeNodeCacheEntry<T>  cacheEntry = getCacheEntry(getId(entity));

            if (cacheEntry == null) {
                log.debug(String.format("Unable to delete entity [%s] as not found in cache.", entity));
                return null;
            }

            TreeNode<T> node = cacheEntry.getNode();
            // remove children of this node if it has any...
            // remove from repo
            node.getChildren().forEach(childNode -> removeChildFromRepository(node.getData(), childNode.getData()));
            // remove from cache (note. this also sets all it's children so that they no longer have this node as a parent.
            node.removeChildren();
            // now remove this node from its parent if it has one
            removeChildNode(node);
            // now all hierarchy links removed can remove from repo and cache
            return super.delete(entity);
        }
    }

    @Override
    public void deleteAll() {
        synchronized (cache) {
            removeAllChildrenFromRepository();
            super.deleteAll();
        }
    }

    public void removeChild(T child) {
        synchronized (cache) {
            TreeNode<T> childNode = getCacheEntry(getId(child)).getNode();
            if (childNode != null) {
                removeChildNode(childNode);
            }
        }
    }

    public void removeChild(ID childId) {
        synchronized (cache) {
            TreeNode<T> childNode = getCacheEntry(childId).getNode();
            if (childNode != null) {
                removeChildNode(childNode);
            }
        }
    }

    private void removeChildNode(TreeNode<T> childNode) {
        TreeNode<T> parentNode = childNode.getParent();
        if (parentNode != null) {
            removeChildFromRepository(parentNode.getData(), childNode.getData());
            parentNode.removeChildNode(childNode);
        }
    }


    public void removeAllChildren() {
        synchronized (cache) {
            removeAllChildrenFromRepository();
            // now clear child/parent relations from the cache
            getCacheEntries().forEach(entry -> entry.getNode().clear());
        }
    }

    private void removeAllChildrenFromRepository() {
        String sql = (hierachyInSameTable) ? REMOVE_ALL_CHILDREN_SAME_TABLE_SQL : REMOVE_ALL_CHILDREN_SEPARATE_TABLE_SQL;
        try {
            executeSqlStatement(sql);
        } catch (SQLException e) {
            throw new RepositoryException("Error removing all child records from the repository using SQL: " + sql);
        }
    }


    private void removeChildFromRepository(T parent, T child) {
        if (hierachyInSameTable) {
            removeChildFromRepository(parent, child, REMOVE_CHILD_SAME_TABLE_SQL);
        } else {
            removeChildFromRepository(parent, child, REMOVE_CHILD_SEPARATE_TABLE_SQL);
        }
    }


    private void removeChildFromRepository(T parent, T child, String usingSQL) {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(usingSQL)
            ) {
                setHierachicalParameterValues(ps, parent, child, SqlOperation.DELETE);
                ps.executeUpdate();
            }
        } catch (SQLException e) {
            throw new RepositoryException(String.format("Error removing child from repository. Parent entity:%s, child entity:%s , using SQL %s", parent, child, usingSQL), e);
        }
    }


    // todo: note need to detect loops. Just need to check when adding a child that the ancestors do not already contain this child.

    /**
     * Sets up a parent child relationship between two entities.
     * The relationship is recorded in the repository and in the cache.
     *
     * @param parent
     * @param child
     * @throws ChildAlreadyHasParentException if the child already has a parent that is different than this one.
     * @throws RepositoryException            if parent and child are the same entity.
     */
    public void addChild(T parent, T child) throws RepositoryException {
        if (parent == null || child == null)
            throw new NullPointerException(String.format("parent and child are both required and should not be be null parent:[%s], child[%s].", parent, child));

        // ensure parent and child are already saved to the repository.
        save(parent);
        save(child);
        addChild(getId(parent), getId(child));
    }


    public void addChild(ID parentId, ID childId) {
        if (parentId == null || childId == null)
            throw new NullPointerException(String.format("parentId and childId are both required and should not be null parent:[%s], child[%s].", parentId, childId));

        synchronized (cache) {
            // may need to remove existing parent child relationship i.e. if this child has an existing parent and is now being adopted by a new parent
            TreeNodeCacheEntry<T> parentEntry = getCacheEntry(parentId);
            TreeNodeCacheEntry<T> childEntry = getCacheEntry(childId);

            if (parentEntry == null || childEntry == null) {
                if (parentEntry == null) {
                    log.debug(String.format("Parent entry for [%s] not found in cache.", parentId));
                }

                if (parentEntry == null) {
                    log.debug(String.format("Child entry for [%s] not found in cache.", childId));
                }

                return;
            }


            TreeNode<T> parentNode = getCacheEntry(parentId).getNode();
            TreeNode<T> childNode = getCacheEntry(childId).getNode();

            if (parentNode == null || childNode == null) return;

            if (childNode.getParent() != null) {
                // if current parent is not equal to this parent then error... need to remove current parent relationship first.
                if (!childNode.getParent().equals(parentNode))
                    throw new ChildAlreadyHasParentException(String.format("Cannot add child entity [%s] as child of parent entity [%s] as it is already a child of [%s].  Use removeChild() method first.", childNode, parentNode, childNode.getParent()));
                else return; // relationship is already set up
            }

            addChildInRepository(parentNode.getData(), childNode.getData());
            parentNode.addChildNode(childNode);
        }
    }

    private void addChildInRepository(T parent, T child) {
        if (hierachyInSameTable) {
            addChildInRepository(parent, child, ADD_CHILD_SAME_TABLE_SQL);
        } else {
            addChildInRepository(parent, child, ADD_CHILD_SEPARATE_TABLE_SQL);
        }
    }

    private void addChildInRepository(T parent, T child, String usingSQL) {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(usingSQL)
            ) {
                setHierachicalParameterValues(ps, parent, child, SqlOperation.UPDATE); // should be INSERT
                ps.executeUpdate();

            }
        } catch (SQLException e) {
            throw new RepositoryException(String.format("Error removing child from repository. Parent entity:%s, child entity:%s , using SQL %s", parent, child, usingSQL), e);
        }
    }


    /**
     * Just need to check that when adding a child the ancestors do not already contain this child.
     *
     * @param node
     * @return
     */
    private boolean hasCycle(TreeNode<T> node) {
        if (node == null) return false;

//        TreeNode<T> turtle = node;
//        TreeNode<T> rabbit = node.get

        return true;
    }


    public Optional<T> getParent(T entity) {
        synchronized (cache) {
            TreeNode<T> node = getCacheEntry(getId(entity)).getNode();
            TreeNode<T> parentNode = node.getParent();
            return (parentNode != null) ? Optional.of(newEntity(parentNode.getData())) : Optional.empty();
        }
    }

    public Stream<T> getChildren(T parent) {
        synchronized (cache) {
            TreeNode<T> parentNode = getCacheEntry(getId(parent)).getNode();
            return (parentNode != null) ? parentNode.getChildren().stream().map(TreeNode::getData) : Stream.empty();
        }
    }

    public Stream<T> getDescendants(T parent) {
        synchronized (cache) {
            TreeNode<T> parentNode = getCacheEntry(getId(parent)).getNode();
            return (parentNode != null) ? parentNode.descendants().map(TreeNode::getData) : Stream.empty();
        }
    }

    private TreeNodeCacheEntry<T> getCacheEntry(ID id) {
        return (TreeNodeCacheEntry<T>) cache.get(id);
    }

    private Stream<TreeNodeCacheEntry<T>> getCacheEntries() {
        return cache.values().stream().map(entry -> (TreeNodeCacheEntry<T>) entry);
    }

    public Stream<T> getAncestors(T entity) {
        synchronized (cache) {
            TreeNode<T> node = getCacheEntry(getId(entity)).getNode();
            return (node != null) ? node.ancestors().map(TreeNode::getData) : Stream.empty();
        }
    }

    public boolean isRoot(T entity) {
        if (entity == null) return false;
        synchronized (cache) {
            TreeNode<T> node = getCacheEntry(getId(entity)).getNode();
            return (node != null) && node.isRoot();
        }
    }

    public boolean isChild(T entity) {
        if (entity == null) return false;
        synchronized (cache) {
            TreeNode<T> node = getCacheEntry(getId(entity)).getNode();
            return (node != null) && node.isChild();
        }

    }

    public boolean isLeaf(T entity) {
        if (entity == null) return false;
        synchronized (cache) {
            TreeNode<T> node = getCacheEntry(getId(entity)).getNode();
            return (node != null) && node.isLeaf();
        }
    }

    private static class TreeNodeCacheEntry<T> implements CacheEntry<T> {

        private TreeNode<T> node;

        public TreeNode<T> getNode() {
            return node;
        }

        public TreeNodeCacheEntry(T entity) {
            node = new TreeNode<T>(entity);
        }

        @Override
        public T get() {
            return node.getData();
        }

        @Override
        public void set(T entity) {
            node.setData(entity);
        }
    }

}
