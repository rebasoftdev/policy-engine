package net.rebasoft.data;

import java.io.Serializable;

/**
 * Created by chriswhiteley on 25/04/2017.
 */
public interface ServiceJson<T, ID extends Serializable> {
    void setRepository(RepositoryJson<T, ID> repository);

    void updateFromRmcJson(String jsonString) throws Exception;

    void updateFromJson(String jsonString) throws Exception;

    String getAllAsJson() throws Exception;

    void deleteFromRmcJson(String jsonString) throws Exception;

    void deleteFromJson(String jsonString) throws Exception;
}
