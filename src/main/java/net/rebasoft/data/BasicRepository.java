package net.rebasoft.data;

import java.io.Serializable;

/**
 * Provides basic read write repo methods to read a single entity by id and save an entity.
 *
 * Created by chriswhiteley on 10/05/2017.
 */
public interface BasicRepository<T,ID extends Serializable> extends Repository<T, ID> {
    /**
     * Retrieves and entity by its id.
     * @param id The id.
     * @return the retrieved entity or null if no entity was found.
     */
    T findOne(ID id);

    /**
     * Save a single entity to the repo.
     *
     * @param entity
     * @param <S>
     * @return
     */
    <S extends T> S save(S entity);

}
