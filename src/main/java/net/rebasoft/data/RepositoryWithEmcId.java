package net.rebasoft.data;

import java.io.Serializable;

/**

 * Repository interface for handling entities with EmcId
 *
 * Created by chriswhiteley on 07/12/2016.
 * @param <ID>
 */
public interface RepositoryWithEmcId <ID extends Serializable> {
    ID getIdFromEmcId(long emcId);
}
