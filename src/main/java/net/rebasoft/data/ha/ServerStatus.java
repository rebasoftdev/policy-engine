package net.rebasoft.data.ha;

/**
 * Created by chriswhiteley on 27/09/2016.
 */
public enum ServerStatus { MASTER_WITH_STANDBY(true, true), MASTER_NO_STANDBY(true, true), STANDBY_WITH_MASTER(false, false), STANDBY_NO_MASTER(true, true), STANDALONE(true, false);

    private final boolean active;
    private final boolean activeHa;

    public boolean isActiveServer() {
        return active;
    }
    public boolean isActiveHAServer() { return activeHa;}

    /**
     * Returns an id as the first part of the name i.e. MASTER_WITH_STANDBY would be "MASTER".
     *
     * @return
     */
    public String getId() {
        switch (this) {
            case MASTER_NO_STANDBY:
            case MASTER_WITH_STANDBY: return "MASTER";
            case STANDBY_NO_MASTER:
            case STANDBY_WITH_MASTER: return "STANDBY";
            case STANDALONE:return "STANDALONE";
        }
        return "";
    }

    /**
     * Returns an id for the peer so if this is a MASTER will return "STANDBY"
     * @return
     */
    public String getPeerId() {
        switch (this) {
            case MASTER_NO_STANDBY:
            case MASTER_WITH_STANDBY: return "STANDBY";
            case STANDBY_NO_MASTER:
            case STANDBY_WITH_MASTER: return "MASTER";
            case STANDALONE:return ""; // no peer for a standalone.
        }
        return "";
    }


    ServerStatus(boolean active, boolean activeHa) {
        this.active = active;
        this.activeHa = activeHa;
    }
}
