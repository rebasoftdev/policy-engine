/**
 * Package that contains classes to do with HA (High Availability)
 *
 * todo: Plan is to refactor and move net.rebasoft.katan.master to here (losing the master package/directory).
 *
 * Created by chriswhiteley on 31/10/2016.
 */
package net.rebasoft.data.ha;