package net.rebasoft.data.ha;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Uses Service Provider pattern.
 *
 * Created by chriswhiteley on 19/07/2016.
 */
public class MapSyncers {
    private MapSyncers() {} // prevent instantiation

    // maps map names to sycers.
    private static final Map<String, HaSyncerProvider> providers = new ConcurrentHashMap<>();
    public static final String DEFAULT_PROVIDER_NAME = "default";

    // Provider Registration API
    public static void registerDefaultProvider (HaSyncerProvider p) {
        registerProvider(DEFAULT_PROVIDER_NAME, p);
    }

    public static void registerProvider (String name, HaSyncerProvider p)  {
        providers.put(name,p);
    }


    // Service Access API
    public static HaSyncer newInstance() {
        return newInstance(DEFAULT_PROVIDER_NAME);
    }

    public static HaSyncer newInstance(String name) {
        HaSyncerProvider p = providers.get(name);
        if (p== null) throw new IllegalArgumentException("No MapSyncerProvider registered with name: " + name);
        return p.newSyncer();
    }

}
