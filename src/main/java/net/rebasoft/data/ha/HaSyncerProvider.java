package net.rebasoft.data.ha;

/**
 * Created by chriswhiteley on 19/07/2016.
 */
public interface HaSyncerProvider {
    HaSyncer newSyncer();
}
