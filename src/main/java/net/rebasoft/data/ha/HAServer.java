package net.rebasoft.data.ha;

import java.util.Collection;

/**
 * Created by chriswhiteley on 05/04/2017.
 */
public interface HAServer {
    ServerStatus getStatus();

    /**
     * Returns a string id to identify this Server for HA purposes
     *
     * @return
     */
    String getId();

    /**
     * Returns a collection of ids that identify the peers of this Server for HA purposes.
     *
     * @return
     */
    Collection<String> getPeerIds();
}
