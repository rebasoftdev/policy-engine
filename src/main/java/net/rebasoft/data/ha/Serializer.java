package net.rebasoft.data.ha;

import com.hazelcast.nio.ObjectDataInput;
import com.hazelcast.nio.ObjectDataOutput;

import java.io.IOException;

/**
 * Created by chriswhiteley on 05/04/2017.
 */
public interface Serializer <T> {
    Class typeClass();
    T read(ObjectDataInput inp) throws IOException;
    void write(ObjectDataOutput out, T obj) throws IOException;
}
