package net.rebasoft.data.ha;

/**
 * Interface that provides the functionality to update HA peers with changes to data values.
 *
 * @param <V> The Value type that contains the data changes to be sent to HA peers.
 */

public interface PeerUpdater <V> {

    /**
     * Send a new create value to HA peers.
     * @param value Value type containing the newly created data.
     */
    void sendCreateToPeers(V value);

    /**
     * Send a new update value to HA peers.
     * @param value Value type containing the new update.
     */
    void sendUpdateToPeers(V value);

    /**
     * Send a new delete to HA peers.
     * @param value Value type containing the deleted value.
     */
    void sendDeleteToPeers(V value);

}
