package net.rebasoft.data.ha;

/**
 * Created by chriswhiteley on 19/07/2016.
 */
public interface HaSyncer {
    /**
     * Sync HA data with data from the repository.
     */
    void sync();
}
