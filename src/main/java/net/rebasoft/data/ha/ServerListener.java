package net.rebasoft.data.ha;

/**
 * Created by chriswhiteley on 27/09/2016.
 */
public interface ServerListener {
    void stateChanged(ServerStatus state);
}
