package net.rebasoft.data;

import java.io.Serializable;

/**
 * Created by chriswhiteley on 25/04/2017.
 */
public interface HATreeService<T, ID extends Serializable & Comparable<ID>> extends HAService<T, ID>, RepositoryTree<T,ID> {
}
