package net.rebasoft.data;

import com.hazelcast.core.IMap;
import com.hazelcast.map.listener.EntryAddedListener;
import com.hazelcast.map.listener.EntryRemovedListener;
import com.hazelcast.map.listener.EntryUpdatedListener;
import net.rebasoft.data.ha.HAServer;
import net.rebasoft.data.ha.HaSyncer;
import net.rebasoft.data.ha.ServerStatus;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;
import java.util.stream.StreamSupport;

/**
 * Abstract class to provide standard Service layer methods. Note for implementation of HA (PeerUpdater interface) it is
 * using the standard Hazelcast IMap approach that has traditionally been used.  If another approach is required these
 * methods should be overridden or a new class (implementation) created.
 * <p>
 * Created by chriswhiteley on 08/11/2016.
 */
public abstract class AbstractHAService<T, ID extends Serializable & Comparable<ID>> extends AbstractHAServiceJson<T, ID> implements HAService<T, ID> {

    private static final Logger log = Logger.getLogger("AbstractServiceWithHA");


    protected IMap<ID, T> haMap;
    private String entryAddedListenerId;
    private String entryRemovedListenerId;
    private String entryUpdatedListenerId;
    protected HAServer haServer;

    public AbstractHAService() {
    }

    public AbstractHAService(RepositoryJsonRUD<T, ID> repository, IMap<ID, T> haMap, HAServer haServer) {
        super(repository);
        setHaMap(haMap);
        startHa(haServer);
    }

    public void setHaMap(IMap<ID, T> haMap) {
        log.debug("Setting HA Map for entity class " + repository.getTypeClass());
        this.haMap = haMap;
    }


    public void startHa(HAServer haServer) {
        log.debug("Setting HA Server as " + haServer + " for entity class " + repository.getTypeClass());
        this.haServer = haServer;

        switch (haServer.getStatus()) {
            case MASTER_WITH_STANDBY:
                // sleep for 1 second before syncing to allow Standby server to setup haMap listeners
                startSync(1);
                break;
            // todo: should STANDALONE be doing syncing?
            case MASTER_NO_STANDBY:
//            case STANDALONE:
                startSync();
                break;
            case STANDBY_WITH_MASTER:
                addHaListeners();
                startSyncRepoToHaMap();
                break;
            case STANDBY_NO_MASTER:
                // ensure sequence is greater than max id. // should also do this at startup (in repo constructor)...
                if (getRepository().usesSequence()) getRepository().alignSequence();
                removeHaListeners();
                startSync();
                break;
            default:
                break;
        }

    }

    private void startSyncRepoToHaMap() {
        Runnable syncTask = this::syncRepoToHaMap;
        new Thread(syncTask).start();
    }

    private void startSync() {
        Runnable syncTask = this::sync;
        new Thread(syncTask).start();
    }


    private void startSync(int initialDelay) {
        Runnable syncTask = () -> {
            try {
                TimeUnit.SECONDS.sleep(initialDelay);
                sync();
            } catch (InterruptedException e) {
                log.warn("syncing interrupted");
            }

        };
        new Thread(syncTask).start();
    }


    /**
     * This is called when the state of this server has changed from either ACTIVE to STANDBY or STANDBY to ACTIVE.
     * <p>
     * If this server has just gone ACTIVE this means it could have been a STANDBY server and if it uses a sequence this
     * could be behind the latest id so first this needs to be brought back into alignment.  Then we remove the listeners
     * from the HA map otherwise any changes we make will cause the listeners to fire.
     * <p>
     * If this server has just gone to STANDBY it means it originally was a STANDBY server that went ACTIVE but has now
     * gone back to STANDBY.  If it uses a sequence then this should be in alignment with the last id but we will need
     * to re add the listeners to the HA map so that any changes made by the MASTER server are replicated.
     * *
     *
     * @param status The ServerState this server has just gone to.
     */
    @Override
    public void stateChanged(ServerStatus status) {
        log.debug("Received Server Status change to " + status + " for entity class " + repository.getTypeClass());
        switch (status) {
            case MASTER_WITH_STANDBY:
                // sleep for 1 second to allow Standby server to setup haMap listeners
                startSync(1);
                break;
            case MASTER_NO_STANDBY:
                // must have just lost Stand By (no worries continue on own)
                break;
            case STANDBY_WITH_MASTER:
                // gone back to standby (slave)
                addHaListeners();
                startSyncRepoToHaMap();
                break;
            case STANDBY_NO_MASTER:
                // ensure sequence is greater than max id. // should also do this at startup (in repo constructor)...
                // note there is not always a sequence (if idAutoGenerated then will have sequence).
                if (getRepository().usesSequence()) getRepository().alignSequence();
                removeHaListeners();
                break;
            case STANDALONE:
            default:
                break;
        }

    }


    @Override
    public void refreshCache() {
        if (repository instanceof Cached) {
            ((Cached) repository).refresh();
            if (haServer.getStatus().isActiveServer()) startSync();
        }
    }


    /**
     * Method to return the hapMap.  In general there is just one map object but for certain entities there could be a
     * specific map depending on the entity so need to be able to select this.\
     *
     * @return
     */
    protected IMap<ID, T> getHaMap(T entity) {
        return haMap;
    }


    public void setRepository(RepositoryJsonRUD<T, ID> repository) {
        this.repository = repository;
    }


    protected RepositoryJsonRUD<T, ID> getRepository() {
        return (RepositoryJsonRUD<T, ID>) repository;
    }

    @Override
    public Optional<T> findOne(ID id) {
        return getRepository().findOne(id);
    }

    @Override
    public Iterable<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        return getRepository().findAll(ids);
    }

    @Override
    public long count() {
        return getRepository().count();
    }

    @Override
    public boolean exists(ID id) {
        return getRepository().exists(id);
    }

    @Override
    public <S extends T> S save(S entity) {
        // save to the repository
        S updated = getRepository().save(entity);

        // update HA Peers
        sendUpdateToPeers(updated);
        return updated;
    }

    @Override
    public <S extends T> Iterable<S> save(Iterable<S> entities) {
        // save to the repository
        Iterable<S> updated = getRepository().save(entities);

        // update HA Peers
        for (S s : updated) {
            sendUpdateToPeers(s);
        }

        return updated;
    }


    @Override
    public void deleteById(ID id) {
        Optional<T> entityOptional = getRepository().findOne(id);
        entityOptional.ifPresent(entity -> {
            getRepository().deleteById(id);
            sendDeleteToPeers(entity);
        });

    }


    @Override
    public <S extends T> S delete(T entity) {
        S deleted = getRepository().delete(entity);
        sendDeleteToPeers(entity);
        return deleted;
    }


    @Override
    public Collection<? extends T> delete(Iterable<? extends T> entities) {
        Collection<? extends T> deleted = getRepository().delete(entities);

        for (T entity : entities) {
            sendDeleteToPeers(entity);
        }

        return deleted;
    }

    @Override
    public void deleteAll() {
        Iterable<T> entities = getRepository().findAll();

        getRepository().deleteAll();

        for (T entity : entities) {
            sendDeleteToPeers(entity);
        }
    }


    protected void addHaListeners() {
        log.debug("Adding HA map listeners to map for entity class " + repository.getTypeClass());


        if (haMap != null) {

            /**
             * AC-551: Hazelcast upgrade
             * addEntryListener has been deprecated
             * http://docs.hazelcast.org/docs/latest/javadoc/com/hazelcast/map/listener/MapListener.html
             */
            entryAddedListenerId = haMap.addEntryListener((EntryAddedListener<ID, T>) entryEvent -> {
                if (haServer.getStatus() == ServerStatus.STANDBY_WITH_MASTER) {
                    T entity = entryEvent.getValue();
                    getRepository().saveNoSequence(entity);
                    log.debug("HA map listener entryAdded, entity saved to repository " + entity);
                }
            }, true);


            entryRemovedListenerId = haMap.addEntryListener((EntryRemovedListener<ID, T>) entryEvent -> {
                if (haServer.getStatus() == ServerStatus.STANDBY_WITH_MASTER) {
                    ID key = entryEvent.getKey();
                    getRepository().deleteById(key);
                    log.debug("HA map listener entryRemoved, entity deleted from repository with id " + key + " for entity type " + repository.getTypeClass());
                }
            }, true);

            entryUpdatedListenerId = haMap.addEntryListener((EntryUpdatedListener<ID, T>) entryEvent -> {
                if (haServer.getStatus() == ServerStatus.STANDBY_WITH_MASTER) {
                    T entity = entryEvent.getValue();
                    getRepository().saveNoSequence(entity);
                    log.debug("HA map listener entryUpdated, entity saved to repository " + entity);
                }
            }, true);
        } else {
            throw new NullPointerException("Cannot add HA map listeners as haMap is null for " + this.getClass().getSimpleName() );
        }
    }

    protected void removeHaListeners() {
        log.debug("Removing HA Map listeners for entity type " + repository.getTypeClass());
        if (entryAddedListenerId != null) haMap.removeEntryListener(entryAddedListenerId);
        if (entryUpdatedListenerId != null) haMap.removeEntryListener(entryUpdatedListenerId);
        if (entryRemovedListenerId != null) haMap.removeEntryListener(entryRemovedListenerId);
    }

    @Override
    protected T getValueFromJson(JSONObject jsonObject) throws Exception {
        return repository.createEntityFromJson(jsonObject);
    }

    protected abstract ID getId(T entity);


    @Override
    public void sendCreateToPeers(T entity) {
        if (haServer.getStatus().isActiveHAServer()) {
            getHaMap(entity).set(getId(entity), entity);
            log.debug("Updated entity in HA map " + entity);
        }
    }

    @Override
    public void sendUpdateToPeers(T entity) {
        if (haServer.getStatus().isActiveHAServer()) {
            getHaMap(entity).set(getId(entity), entity);
            log.debug("Updated entity in HA map " + entity);
        }
    }

    @Override
    public void sendDeleteToPeers(T entity) {
        if (haServer.getStatus().isActiveHAServer()) {
            getHaMap(entity).delete(getId(entity));
            log.debug("Deleted entity from HA map " + entity);
        }
    }


    /**
     * todo: Note. this is a private method but could make it public and run this on a regular basis
     * to keep things in check!
     */
    synchronized void syncRepoToHaMap() {
        log.debug("Syncing repo with HA map");

        // query all entries from repo
        Iterable<T> repoEntities = findAll();

        // check in the repo for entries that have been deleted from the HA map and update any existing entries
        repoEntities.forEach(entity -> {
            if (haMap.containsKey(getId(entity))) {
                log.debug("Entity is in the HA map and the repository, update it in the repository " + entity);
                getRepository().saveNoSequence(entity);
            } else {
                log.debug("Entity not in the HA map, so removing from the repository " + entity);
                getRepository().delete(entity);
            }
        });

        // now check for new inserts (are there any entries in the HA map that we do not have in the repository?)
        haMap.entrySet().stream().filter(entry -> !findOne(entry.getKey()).isPresent()).forEach(entry -> {
            log.debug("Entity is in the HA map but not in the repository, adding to the repository " + entry.getValue());
            getRepository().saveNoSequence(entry.getValue());
        });

    }


    @Override
    public synchronized void sync() {
        log.debug("Syncing HA map with repo.");

        // check in the map for entries update any existing in the database and remove any that have been deleted (don't exist in the database)
        for (T entity : haMap.values()) {
            Optional<T> repoEntityOptional = findOne(getId(entity));

            if (repoEntityOptional.isPresent()) {
                log.debug("Entity is in the repository and in the HA map, update it in the HA map " + repoEntityOptional.get());
                haMap.set(getId(repoEntityOptional.get()), repoEntityOptional.get());

            } else {
                // not in the repo so remove it from the map
                log.debug("Entity not in the repository, so removing from the HA map " + entity);
                haMap.remove(entity);

            }
        }

        // query all entries from repo
        Iterable<T> repoEntities = findAll();

        // now check for new inserts (are there any entries in the repo that we do not have in the map?)
        StreamSupport.stream(repoEntities.spliterator(), false).filter(e -> !haMap.containsKey(getId(e))).forEach(e -> {
            log.debug("Entity is in the repository but not in the HA map, adding to the HA map " + e);
            haMap.set(getId(e), e);
        });
        log.debug("Syncing HA map with repo completed.");
    }

    @Override
    public HaSyncer newSyncer() {
        return this;
    }


    @SuppressWarnings("unused, unchecked")
    public ID getIdFromEmcId(long emcId) {

        if (repository instanceof RepositoryWithEmcId) {
            return ((RepositoryWithEmcId<ID>) repository).getIdFromEmcId(emcId);
        } else {
            return null;
        }
    }

    @SuppressWarnings("unused")
    public void resetAllEmcIds() {
        List<T> updated = new ArrayList<>();

        for (T entity : findAll()) {
            if (entity instanceof EntityWithEmcId) {
                ((EntityWithEmcId) entity).setEmcId(-1);
                updated.add(entity);
            }
        }

        if (!updated.isEmpty()) {
            save(updated);
            // refresh the cache since part of the cache is built on the emcId to id mapping.
            refreshCache();
        }

    }


    @Override
    public <S extends T> S saveNoSequence(S entity) {
        return getRepository().saveNoSequence(entity);
    }
}
