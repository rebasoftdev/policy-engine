package net.rebasoft.data;

/**
 * Created by chriswhiteley on 17/11/2016.
 */
public interface EntityWithEmcId {
    void setId(long id);
    long getId();
    void setEmcId(long emcid);
    long getEmcId();
}
