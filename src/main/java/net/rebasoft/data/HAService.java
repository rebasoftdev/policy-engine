package net.rebasoft.data;

import net.rebasoft.data.ha.HaSyncer;
import net.rebasoft.data.ha.HaSyncerProvider;
import net.rebasoft.data.ha.ServerListener;


import java.io.Serializable;

/**
 * Created by chriswhiteley on 25/04/2017.
 */
public interface HAService<T, ID extends Serializable & Comparable<ID>> extends DataService<T, ID>, ServerListener, HaSyncer, HaSyncerProvider {

}
