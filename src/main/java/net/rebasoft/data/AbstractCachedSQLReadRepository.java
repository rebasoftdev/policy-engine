package net.rebasoft.data;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

/**
 * Created by chriswhiteley on 07/11/2016.
 */
public abstract class AbstractCachedSQLReadRepository<T, ID extends Serializable> implements RepositoryR<T, ID> {
    private static final Logger log = Logger.getLogger("AbstractRepository");
    private Map<ID, T> cache;
    private final String SELECT_ALL_SQL = "SELECT " + columnList() + " FROM " + tableName();
    private DataSource dataSource;

    public AbstractCachedSQLReadRepository(DataSource dataSource) {
        this.dataSource = dataSource;
        cache = new HashMap<>();
        // populate the cache in a separate thread
        Runnable populateTask = () -> {
            populateCache();
        };
        new Thread(populateTask).start();
    }

    /**
     * Populate the cache in a separate thread.
     */
    private void populateCache() {
        try {
            try (Connection conn = dataSource.getConnection();
                 Statement statement = conn.createStatement()
            ) {
                ResultSet rs = statement.executeQuery(SELECT_ALL_SQL);
                while (rs.next()) {
                    T entity = getEntityFromResultSet(rs);
                    cache.put(getId(entity), entity);
                }
            }
        } catch (SQLException e) {
            throw new RepositoryException(String.format("Error populating cache using query %s", SELECT_ALL_SQL), e);
        }
    }

    /**
     * Method that returns the table name for use in underlying SQL statements.
     *
     * @return
     */
    protected abstract String tableName();


    /**
     * This should provide the comma separated list of column names starting with the key column
     *
     * @return
     */
    protected abstract String columnList();


    /**
     * Serializes the entity as a JSON object.
     *
     * @param entity
     * @return
     */
    protected abstract JSONObject asJsonObeject(T entity) throws JSONException;


    /**
     * This extracts the data from the ResultSet {@code rs}.
     * The data in the ResultSet will be in the order listed in {@code columnList()}
     * <p>
     * For example, if columnList() returns:
     * <p>
     * id, name, script, emcid
     * <p>
     * <p>
     * then the code to set up the entity values should be someting like:
     * <p>
     * entity.setId(ps.getLong("id"));
     * entity.setName(ps.getString("name"));
     * entity.setScript(ps.getString("script"));
     * entity.setEmcId(ps.getInt("emcId"));
     *
     * @param rs
     * @param <S>
     * @return
     * @throws SQLException
     */
    public abstract <S extends T> S getEntityFromResultSet(ResultSet rs) throws SQLException;


    /**
     * Gets the Id value from the entity.
     *
     * @param entity
     * @return
     */
    protected abstract ID getId(T entity);


    @Override
    public Optional<T> findOne(ID id) {
        return (cache.get(id) == null) ? Optional.empty() : Optional.of(cache.get(id));
    }


    @Override
    public Iterable<T> findAll() {
        return cache.values();
    }


    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        Collection<T> entities = new ArrayList<>();

        for (ID id : ids) {
            Optional<T> entityOptional = findOne(id);
            entityOptional.ifPresent(entity -> entities.add(entity));
        }

        return entities;
    }


    @Override
    public long count() {
        return cache.size();
    }

    @Override
    public boolean exists(ID id) {
        return cache.containsKey(id);
    }


    public String getAllAsJson() throws JSONException {
        JSONArray jsonArray = new JSONArray();
        for (T entity : cache.values()) {
            JSONObject e = asJsonObeject(entity);
            jsonArray.put(e);
        }

        return jsonArray.toString();
    }


}
