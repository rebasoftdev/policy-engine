package net.rebasoft.data;

/**
 * Created by chriswhiteley on 02/09/2016.
 */

import java.io.IOException;
import java.io.LineNumberReader;
import java.io.Reader;
import java.sql.*;

/**
 * Tool to run database scripts
 */
public class ScriptRunner {
    private static final org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger("ScriptRunner");

    private static final String DEFAULT_DELIMITER = ";";
    private static final String LS = System.getProperty("line.separator");

    private final Connection connection;

    private final boolean stopOnError;
    private final boolean autoCommit;
    private String delimiter = DEFAULT_DELIMITER;
    private boolean fullLineDelimiter = false;
    private String functionDelimiter = "";

    /**
     * Default constructor
     */
    public ScriptRunner(Connection connection, boolean autoCommit,
                        boolean stopOnError) {
        this.connection = connection;
        this.autoCommit = autoCommit;
        this.stopOnError = stopOnError;
    }

    public void setDelimiter(String delimiter, boolean fullLineDelimiter) {
        this.delimiter = delimiter;
        this.fullLineDelimiter = fullLineDelimiter;
    }


    /**
     * Runs an SQL script (read in using the Reader parameter)
     *
     * @param reader - the source of the script
     */
    public void runScript(Reader reader) throws IOException, SQLException {
        try {
            boolean originalAutoCommit = connection.getAutoCommit();
            try {
                if (originalAutoCommit != this.autoCommit) {
                    connection.setAutoCommit(this.autoCommit);
                }
                runScript(connection, reader);
            } finally {
                connection.setAutoCommit(originalAutoCommit);
            }
        } catch (IOException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        } catch (Exception e) {
            throw new RuntimeException("Error running script.  Cause: " + e, e);
        }
    }

    /**
     * Runs an SQL script (read in using the Reader parameter) using the
     * connection passed in
     *
     * @param conn   - the connection to use for the script
     * @param reader - the source of the script
     * @throws SQLException if any SQL errors occur
     * @throws IOException  if there is an error reading from the Reader
     */
    private void runScript(Connection conn, Reader reader) throws IOException,
            SQLException {
        StringBuffer command = null;
        try {
            LineNumberReader lineReader = new LineNumberReader(reader);
            String line = null;
            while ((line = lineReader.readLine()) != null) {
                if (command == null) {
                    command = new StringBuffer();
                }
                String trimmedLine = line.trim();
                if (trimmedLine.startsWith("--") || trimmedLine.startsWith("//") || trimmedLine.startsWith("#")){
                    if (functionDelimiter.isEmpty()) {
                        log.info(trimmedLine);  // log the comment
                    } else {
                        // comment is part of function definition so include in the command
                        command.append(line);
                        command.append(LS);
                    }
                } else if (trimmedLine.toLowerCase().replaceAll(" +", " ").startsWith("create or replace function") && !trimmedLine.endsWith(getDelimiter())) {
                    functionDelimiter = "$$ language";
                    command.append(line);
                    command.append(LS);
                } else if (trimmedLine.length() < 1) {
                    // Do nothing line commented out
                } else if (!functionDelimiter.isEmpty() && trimmedLine.toLowerCase().replaceAll(" +", " ").startsWith(functionDelimiter)) {
                    functionDelimiter = "";
                    command.append(line);
                    command.append(LS);
                    runCommand(conn, command);
                    command = null;
                    Thread.yield();

                } else if ((functionDelimiter.isEmpty() && !fullLineDelimiter
                        && trimmedLine.endsWith(getDelimiter()))
                        || (functionDelimiter.isEmpty() && fullLineDelimiter
                        && trimmedLine.equals(getDelimiter()))) {
                    command.append(line.substring(0, line
                            .lastIndexOf(getDelimiter())));
                    command.append(LS);
                    runCommand(conn, command);
                    command = null;
                    Thread.yield();
                } else {
                    command.append(line);
                    command.append(LS);
                }
            }
            if (!autoCommit) {
                conn.commit();
            }
        } catch (SQLException e) {
            e.fillInStackTrace();
            log.error("Error executing: " + command, e);
            throw e;
        } catch (IOException e) {
            e.fillInStackTrace();
            log.error("Error executing: " + command, e);
            throw e;
        } finally {
            if (!autoCommit) {
                conn.rollback();
            }
        }
    }

    private void runCommand(Connection conn, StringBuffer command) throws SQLException {
        Statement statement = conn.createStatement();
        log.debug(command);

        boolean hasResults = false;
        if (stopOnError) {
            hasResults = statement.execute(command.toString());
        } else {
            try {
                statement.execute(command.toString());
            } catch (SQLException e) {
                log.error("Error executing: " + command + " " + e.getMessage());
            }
        }

        if (autoCommit && !conn.getAutoCommit()) {
            conn.commit();
        }

        ResultSet rs = statement.getResultSet();
        if (hasResults && rs != null) {
            ResultSetMetaData md = rs.getMetaData();
            int cols = md.getColumnCount();
            for (int i = 0; i < cols; i++) {
                String name = md.getColumnLabel(i);
                log.debug(name + "\t");
            }
            log.debug("");
            while (rs.next()) {
                for (int i = 0; i < cols; i++) {
                    String value = rs.getString(i);
                    log.debug(value + "\t");
                }
                log.debug("");
            }
        }

        try {
            statement.close();
        } catch (Exception e) {
            // Ignore to workaround a bug in Jakarta DBCP
        }
    }

    private String getDelimiter() {
        return delimiter;
    }

}

