package net.rebasoft.data;

import net.rebasoft.data.ha.PeerUpdater;

import java.io.Serializable;

/**
 * Created by chriswhiteley on 25/04/2017.
 */
public interface HAServiceJson<T, ID extends Serializable> extends ServiceJson<T,ID>, PeerUpdater<T> {
}
