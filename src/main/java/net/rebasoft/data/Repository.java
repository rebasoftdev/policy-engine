package net.rebasoft.data;

import java.io.Serializable;

/**
 * Central Repository marker interface. This is to emulate Spring data interface structure.
 * Captures the domain type to manage as well as the domain type's id type.
 *
 * @param <T> the domain type the repository manages
 * @param <ID> the type of the id of the entity the repository manages
 */

public interface Repository <T,ID extends Serializable> {
    Class<T> getTypeClass();
}
