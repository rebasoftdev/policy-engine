package net.rebasoft.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;

/**
 * Created by chriswhiteley on 01/11/2016.
 */
public abstract class AbstractRepositoryJson<T, ID extends Serializable> implements RepositoryJson<T, ID> {

    protected ObjectMapper mapper;
    protected final Class<T> typeParameterClass;

    public AbstractRepositoryJson(Class<T> typeParameterClass) {
        mapper = new ObjectMapper();
        this.typeParameterClass = typeParameterClass;
    }

    public Class<T> getTypeClass() {
        return typeParameterClass;
    }


    public String getAllAsJson() throws Exception {
        JSONArray jsonArray = new JSONArray();
        for (T entity : getAllEntities()) {
            JSONObject e = asJsonObject(entity);
            jsonArray.put(e);
        }

        return jsonArray.toString();
    }

    protected abstract Collection<T> getAllEntities();


    /**
     * Method to convert the entity to a JSONObject.
     *
     * @param entity
     * @return
     */
    protected JSONObject asJsonObject(T entity) throws Exception {
        return new JSONObject(mapper.writeValueAsString(entity));
    }


    /**
     * Converts the JSONObject to the entity type.
     *
     * @param jsonObject
     * @return
     * @throws Exception
     */
    public T createEntityFromJson(JSONObject jsonObject) throws Exception {
        return mapper.readValue(jsonObject.toString(), typeParameterClass);
    }


    @Override
    public Collection<? extends T> updateFromJson(String jsonString) {
        JSONArray jsonArray = getJsonArray(jsonString);
        Collection<? extends T> updates = new HashSet<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                updates.add(updateUsingJsonObject(jsonObject));
            } catch (Exception e) {
                throw new RepositoryException("Error updating using JSON " + jsonString, e);
            }
        }

        return updates;
    }

    protected abstract <S extends T> S updateUsingJsonObject(JSONObject jsonObject) throws Exception;


    @Override
    public Collection<? extends T> deleteFromJson(String jsonString) {
        JSONArray jsonArray = getJsonArray(jsonString);
        Collection<? extends T> deletes = new HashSet<>();

        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                JSONObject jsonObject = jsonArray.getJSONObject(i);
                deletes.add(DeleteUsingJsonObject(jsonObject));
            } catch (Exception e) {
                throw new RepositoryException("Error deleting using JSON " + jsonString, e);
            }
        }

        return deletes;
    }

    protected abstract <S extends T> S DeleteUsingJsonObject(JSONObject jsonObject) throws Exception;


}
