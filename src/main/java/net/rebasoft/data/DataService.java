package net.rebasoft.data;

import java.io.Serializable;

/**
 * Created by chriswhiteley on 25/04/2017.
 */
public interface DataService <T, ID extends Serializable & Comparable<ID>> extends RepositoryR<T,ID>,RepositoryU<T,ID>, RepositoryD<T,ID> {

    void refreshCache();

}
