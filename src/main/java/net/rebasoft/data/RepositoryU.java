package net.rebasoft.data;

import java.io.Serializable;

/**
 * Repository interface with standard update (save) methods.
 *
 * @param <T> The data Type (Entity).
 * @param <ID> The Id (Primary key) type.
 */
public interface RepositoryU  <T, ID extends Serializable> extends Repository<T, ID> {

    /**
     * Save a single entity to the repo. This determines whether the entity is a new entity that needs to be created
     * in the repository or an existing entity that needs to be updated in the repository.  If this is a new entity then
     * the method may allow for the ID to be sequence generated.
     *
     * @param entity
     * @param <S>
     * @return
     */
    <S extends T> S save(S entity);


    /**
     * Save a single entity to the repo with no sequence generation of id on create.  This is as per the previous method but
     * can be used where we don't require auto generation of the id using a sequence.
     *
     * @param entity
     * @param <S>
     * @return
     */
    <S extends T> S saveNoSequence(S entity);


    /**
     * Save a number of entities to the repo.
     * @param entities
     * @param <S>
     * @return
     */
    <S extends T> Iterable<S> save(Iterable<S> entities);

    /**
     * Method should return true if this repo uses a sequence to generate the id.
     * By default this returns false.
     *
     * @return
     */
    default boolean usesSequence() {return false;}


    /**
     * This method should be implemented if usesSequence returns true.  This will ensure that the sequence that is used
     * for this entity type {@code T} is aligned with the primary key values of the data.  For example if the data
     * contains the maximum primary key of 50 but the sequence is currently at 30 then the sequence will be re aligned so
     * the next value method will return 51.  The reason a sequence can get out of alignment is when running in HA mode
     * and the Master box is generating the sequence then the Standby box sequence can get out of alignment i.e. it is not
     * up to date with the Master box.  The first thing to do if a Standby box becomes the active box (when the Master goes
     * down) is to bring the sequence in line with the data.
     */
    default void alignSequence() {
        if (usesSequence()) {
            throw new RepositoryException("This repository uses a sequence, so this method alignSequence() should have a proper implementation.");
        }
    }


}
