package net.rebasoft.data;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import javax.sql.DataSource;
import java.io.Serializable;
import java.sql.*;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Abstract repository class to provide basic Read, Update, Delete functionality using SQL (JDBC).
 * This also extends AbstractRepositoryJson so provides methods for accessing the repository using JSON to retrieve/update/deleteById
 * the data.
 * <p>
 * Created by chriswhiteley on 07/11/2016.
 *
 * @param <T>  The entity type.
 * @param <ID> The primary key type.
 */
public abstract class AbstractCachedSQLRepository<T, ID extends Serializable & Comparable<ID>> extends AbstractRepositoryJson<T, ID> implements RepositoryJsonRUD<T, ID>, RepositoryWithEmcId<ID>, Cached {
    private static final Logger log = Logger.getLogger("AbstractCachedSQLRepository");
    protected final DataSource dataSource;
    private final boolean idSequenceGenerated;
    final Map<ID, CacheEntry<T>> cache;
    Map<Long, ID> emcIdToIdCache;
    private static final String PARAMS = "?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?";
    private final String[] COLUMN_NAMES = columnList().split(",");
    private final String[] NON_ID_COLUMN_NAMES = Arrays.copyOfRange(COLUMN_NAMES, 1, COLUMN_NAMES.length);
    private final String PARAMETER_LIST_WITHOUT_ID = PARAMS.substring(0, NON_ID_COLUMN_NAMES.length + NON_ID_COLUMN_NAMES.length - 1);
    private final String PARAMETER_LIST = PARAMS.substring(0, COLUMN_NAMES.length + COLUMN_NAMES.length - 1);
    private final String INSERT_AUTO_ID_SQL = "INSERT INTO " + tableName() + " (" + columnListWithoutId() + ")  VALUES  (" + PARAMETER_LIST_WITHOUT_ID + ")";
    private final String INSERT_ID_SUPPLIED_SQL = "INSERT INTO " + tableName() + " (" + columnList() + ")  VALUES  (" + PARAMETER_LIST + ")";
    private final String UPDATE_SQL = "UPDATE " + tableName() + "  SET " + updateColumnList() + " WHERE " + updateParameterList();

    private final String DELETE_SQL = "DELETE FROM " + tableName() + " WHERE " + idColumnName() + " =? ";
    private final String DELETE_ALL_SQL = "DELETE FROM " + tableName();
    private final String SELECT_ALL_SQL = "SELECT " + columnList() + " FROM " + tableName();
    private final String SELECT_SEQUENCE_LAST_VALUE_SQL = "SELECT last_value FROM " + sequenceName();
    private final String SET_SEQUENCE_VALUE_SQL = String.format("SELECT setval('%s', ?)", sequenceName());
    private final String SET_SEQUENCE_VALUE_TO_MAX_ID_SQL = String.format("SELECT setval('%s', (SELECT MAX(%s) FROM %s))", sequenceName(), idColumnName(), tableName());
    private final String ALTER_SEQUENCE_RESTART_WITH_SQL = "ALTER SEQUENCE " + sequenceName() + " RESTART WITH %d";

    public AbstractCachedSQLRepository(Class<T> typeParameterClass, DataSource dataSource, boolean idSequenceGenerated) {
        super(typeParameterClass);
        this.dataSource = dataSource;
        this.idSequenceGenerated = idSequenceGenerated;
        this.cache = new HashMap<>(1000);
        emcIdToIdCache = new HashMap<>(1000);

        // populate the cache in a separate a thread.
//        Runnable populateTask = this::populateCache;
//        new Thread(populateTask).start();
//        // just populate the cache in this thread...
        populateCache();
    }


    /**
     * Override this if using database that does not understand the Postgres command "SELECT last_value FROM {@code sequenceName()}
     * to use something that is does understand.
     *
     * @return
     */
    protected String getSequenceLastValueSQL() {
        return SELECT_SEQUENCE_LAST_VALUE_SQL;
    }

    /**
     * Override this if using database that has different SQL command than Postgres for ALTER SEQUENCE {@code sequenceName()}
     * RESTART WITH %d.
     *
     * @param newValue
     * @return
     */
    private String getAlterSequenceValueSQL(long newValue) {
        return String.format(ALTER_SEQUENCE_RESTART_WITH_SQL, newValue);
    }

    /**
     * Populate the cache with all rows in this table.
     */
    void populateCache() {
        synchronized (cache) {
            cache.clear();
            try {
                try (Connection conn = dataSource.getConnection();
                     Statement statement = conn.createStatement()
                ) {
                    ResultSet rs = statement.executeQuery(SELECT_ALL_SQL);
                    while (rs.next()) {
                        T entity = getEntityFromResultSet(rs);
                        updateCache(entity);
                    }
                }
            } catch (Exception e) {
                throw new RepositoryException(String.format("Error populating cache using query %s", SELECT_ALL_SQL), e);
            }
        }
    }

    @Override
    public void refresh() {
        // populate the cache in a separate a thread.
        Runnable populateTask = this::populateCache;
        new Thread(populateTask).start();
    }


    private T getCachedEntity(ID id) {
        CacheEntry<T> entry = cache.get(id);
        return (entry == null) ? null : entry.get();
    }

    <S extends T> void updateCache(S entity) {
        synchronized (cache) {
            ID id = getId(entity);
            T currentEntity = getCachedEntity(id);
            cache.put(id, new DefaultCacheEntry(preUpdateCache(currentEntity, entity)));

            if (entity instanceof EntityWithEmcId) {
                EntityWithEmcId entityWithEmcId = (EntityWithEmcId) entity;

                if (entityWithEmcId.getEmcId() > 0) {
                    emcIdToIdCache.put(entityWithEmcId.getEmcId(), getId(entity));
                }
            }

            log.debug("Entity updated in cache: " + entity);
        }
    }


    /**
     * This method allows subclasses to pre-process an entity before it is put into the cache for allowing them to set
     * extra values on the entity before it is written to the cache.
     *
     * @param oldEntity The existing entity that is in the cache or null if this entity does not currently exist in the cache.
     * @param newEntity The new entity about to be written to the cache.
     * @param <S>
     * @return This should return the entity that should be written to the cache
     */
    protected <S extends T> T preUpdateCache(T oldEntity, S newEntity) {
        return newEntity;
    }


    /**
     * Method that returns the table name for use in underlying SQL statements.
     *
     * @return the name of the table.
     */
    protected abstract String tableName();

    /**
     * Method to return the name of the Sequence.  This will return the name of the sequence using the default naming
     * convention. If this is not the correct naming then the method should be overridden.  Note this method always
     * returns a name for the sequence even if no sequence is set up for this table.  Use the boolean method
     * {@code usesSequence()} to determine if a sequence exists for this table.
     *
     * @return the name of the sequence.
     */
    protected String sequenceName() {
        return tableName() + "_id_seq";
    }


    /**
     * This should provide the comma separated list of column names starting with the key column which should be of type ID
     * To set up versioning the version column name should be the last column name in the list and its type should be long
     * (or whatever type matches long in the database e.g. BIGINT for postgres).
     *
     * @return
     */
    protected abstract String columnList();


    /**
     * This sets the SQL statement parameter values into the PreparedStatement {@code ps}.
     * These should be in the order listed in {@code columnList()}.
     * For operation == INSERT If the ID is sequence generated then this should not include the first column (id column),
     * if the ID is not sequence generated this this should include all columns.
     * <p>
     * <p>
     * For example, for INSERT with ID which is sequence generated and columnList() returns:
     * <p>
     * id, name, script, emcid
     * <p>
     * then the code should be:
     * <p>
     * ps.setString (1, entity.getName());
     * ps.setString (2, entity.getScript());
     * ps.setInt(3,entity.getEmcId());
     * <p>
     * If operation == UPDATE then this should be set up for an update with the ID column being the last column e.g.
     * continuing the above for an update then the code would be:
     * <p>
     * ps.setString (1, entity.getName());
     * ps.setString (2, entity.getScript());
     * ps.setInt(3,entity.getEmcId());
     * ps.setLong(4,entity.getId());
     * <p>
     * If operation == UPDATE and we also have a version column then check the flag incrementVersion to determine if the
     * version should be incremented when saved to the database (but do not update it on the object as this is done separately if the update works).
     * <p>
     * For example, if we have a version column "version" and columnList() returns:
     * <p>
     * id, name, version, script, emcid
     * <p>
     * Then on insert operation version should be set to a value of 1.  But for update (if incrementing version) the code should be.
     * <p>
     * ps.setString (1, entity.getName());
     * long newVersion = (incrementVersion)?entity.getVersion():entity.getVersion() + 1;
     * ps.setLong (2, newVersion)
     * ps.setString (3, entity.getScript());
     * ps.setInt(4,entity.getEmcId());
     * ps.setLong(5,entity.getId());
     * <p>
     * If operation == DELETE then only need the id column:
     * <p>
     * ps.setString(1,entity.getId());
     * <p>
     * Note. operation will never be SELECT so don't need to check for that.
     *
     * @param ps
     * @param entity
     * @param operation
     * @param idSequenceGenerated
     * @param incrementVersion
     * @param <S>
     * @throws SQLException
     */
    public abstract <S extends T> void setParameterValues(PreparedStatement ps, S entity, SqlOperation operation, boolean idSequenceGenerated, boolean incrementVersion) throws Exception;


    /**
     * This extracts the data from the ResultSet {@code rs}.
     * The data in the ResultSet will be in the order listed in {@code columnList()}
     * <p>
     * For example, if columnList() returns:
     * <p>
     * id, name, script, emcid
     * <p>
     * <p>
     * then the code to set up the entity values should be sometHing like:
     * <p>
     * entity.setId(ps.getLong("id"));
     * entity.setName(ps.getString("name"));
     * entity.setScript(ps.getString("script"));
     * entity.setEmcId(ps.getInt("emcId"));
     *
     * @param rs
     * @param <S>
     * @return
     * @throws SQLException
     */
    protected abstract <S extends T> S getEntityFromResultSet(ResultSet rs) throws Exception;


    /**
     * Method to return true if this entity is a new entity (i.e. does not exist in the repository yet).
     * This might be implemented by returning true if the id is, for example null, 0, or less than 0.
     *
     * @param entity
     * @param <S>
     * @return
     */
    protected abstract <S extends T> boolean isNewEntity(S entity);


    protected ID getId(T entity) {
        if (entity instanceof EntityWithEmcId) {
            // if this is an EntityWithEmcId type entity then Obtain the correct id based on what we have for id and emcid.
            EntityWithEmcId entityWithEmcId = (EntityWithEmcId) entity;

            long id = entityWithEmcId.getId();
            long emcId = entityWithEmcId.getEmcId();

            if (emcId > 0) {
                // looks like this is an entity that has come across from RMC so may have the id cached if this is an existing entity.
                Long cachedId = (Long) getIdFromEmcId(emcId);
                if (cachedId != null) id = cachedId;
            }

            return (ID) (Long) id;

        } else {
            // need to override this method.....
            throw new RepositoryException("Entity type is not EntityWithEmcId type. To avoid this error, this getId() method needs to be correctly overridden in the repository implementation class.");
        }
    }


    protected abstract <S extends T> S newEntity(S s);


    /**
     * Sets the id value in the entity.
     * <p>
     * For example for a table with a long PK this should be implemented along the limes of:
     * <p>
     * setId(entity, generatedKeys.getLong(1));
     *
     * @param <S>
     * @param entity
     * @param generatedKey
     * @throws SQLException
     */
    protected abstract <S extends T> void setId(S entity, ResultSet generatedKey) throws SQLException;


    /**
     * Returns the list of column value pairs for use in SQL UPDATE statement.
     * <p>
     * For example if firstname and lastname are column names then
     * <p>
     * firstname=?,lastname=?
     * <p>
     * is an update list to update these in the UPDATE statement.
     * <p>
     * Note. since by convention the first column in the list is the ID column this is not included in the list
     * since this is used in the WHERE clause of the UPDATE statement.
     * Also Note. That if there is a version column to be used on the table (i.e. versionColumnName() method returns non null/empty column name)
     * the this should be last in the column names list.
     *
     * @return
     */
    private String updateColumnList() {

        StringBuilder list = new StringBuilder();

        for (int i = 0; i < NON_ID_COLUMN_NAMES.length; i++) {
            list.append(NON_ID_COLUMN_NAMES[i]);
            list.append("=?,");
        }

        return list.substring(0, list.length() - 1);
    }


    /**
     * Returns this list of column names for the SQL insertIdAutoGenerated statement.
     * Note this does not include the id column as this will be autogenrated by when the SQL is run.
     *
     * @return
     */
    private String columnListWithoutId() {
        return String.join(",", NON_ID_COLUMN_NAMES);
    }


    /**
     * Returns the name of the ID column which by convention is the first column in the list returned by the method
     * {@code columnList()}
     *
     * @return
     */
    String idColumnName() {
        return COLUMN_NAMES[0];
    }


    private String updateParameterList() {
        StringBuilder paramList = new StringBuilder();
        paramList.append(idColumnName()).append("=?");
//        if (versionColumnName()!= null && !versionColumnName().isEmpty()) {
//            paraList.append(" AND ").append(versionColumnName()).append("=?");
//        }
        return paramList.toString();
    }


    @Override
    public <S extends T> S save(S entity) {
        return save(entity, supportsVersioning());
    }


    @Override
    public <S extends T> S save(S entity, boolean incrementVersion) {
        S updatedEntity = entity;
        synchronized (cache) {
            if (isNew(entity)) {
                if (idSequenceGenerated && isNewEntity(entity)) {
                    updatedEntity = insertIdAutoGenerated(entity);
                } else {
                    updatedEntity = insertIdSupplied(entity);
                }
            } else {
                // this is an update check if the entity has changes that require an update of the repo
                T currentEntity = cache.get(getId(entity)).get();

                if (entityHasRepoUpdates(currentEntity, entity)) {
                    updatedEntity = update(entity, incrementVersion);
                }
            }

            updateCache(updatedEntity);
            return updatedEntity;
        }
    }


    @Override
    public <S extends T> S saveNoSequence(S entity) {
        return saveNoSequence(entity, supportsVersioning());
    }


    /**
     * Save a single entity to the repo with no sequence generation of id on create.  This is as per the previous method but
     * can be used where we don't require auto generation of the id using a sequence.
     *
     * @param entity
     * @param incrementVersion
     * @param <S>
     * @return
     */
    public <S extends T> S saveNoSequence(S entity, boolean incrementVersion) {
        S updatedEntity = entity;

        synchronized (cache) {
            if (isNew(entity)) {
                updatedEntity = insertIdSupplied(entity);
            } else {
                // this is an update check if the entity has changes that require an update of the repo
                T currentEntity = cache.get(getId(entity)).get();

                if (entityHasRepoUpdates(currentEntity, entity)) {
                    updatedEntity = update(entity, incrementVersion);
                }
            }

            updateCache(updatedEntity);
            return updatedEntity;
        }
    }

    /**
     * By default it is assumed that a call of the save() method will always update the repository but for some entities
     * they can have attributes that are not required to be persisted to the repository.
     * <p>
     * In such cases this method can be overridden to return false when there are no changes to repository attributes
     * and the only changes are to those attributes that don't require persisting and are just held in memory.  In this
     * case the method should return false and only the cache will be updated and not the repository.
     *
     * @param currentEntity
     * @param newEntity
     * @param <S>
     * @return
     */
    protected <S extends T> boolean entityHasRepoUpdates(S currentEntity, S newEntity) {
        return true;
    }


    /**
     * Method should return true if this repo uses a sequence to generate the id.
     * By default this returns false.
     *
     * @return
     */
    public boolean usesSequence() {
        return idSequenceGenerated;
    }


    /**
     * This method should be implemented if usesSequence returns true.  This will ensure that the sequence that is used
     * for this entity type {@code T} is aligned with the primary key values of the data.  For example if the data
     * contains the maximum primary key of 50 but the sequence is currently at 30 then the sequence will be re aligned so
     * the next value method will return 51.  The reason a sequence can get out of alignment is when running in HA mode
     * and the Master box is generating the sequence then the Standby box sequence can get out of alignment i.e. it is not
     * up to date with the Master box.  The first thing to do if a Standby box becomes the active box (when the Master goes
     * down) is to bring the sequence in line with the data.
     */
    public synchronized void alignSequence() {
        if (usesSequence()) {
            if (!cache.isEmpty()) { // note need to check first that the cache is not empty otherwise get NoSuchElementException on call to lastKey() method.
                Long lastId = 0L;

                Object maxId = Collections.max(cache.keySet());
                if (maxId instanceof Integer) {
                    lastId = new Long((Integer) maxId);
                } else if (maxId instanceof Long) {
                    lastId = (Long) maxId;
                } else {
                    throw new IllegalStateException("Cannot align sequence for sequences of type " + maxId.getClass());
                }
                // get the current value of the sequence
                Long sequenceCurrentValue = getSequenceCurrentValue();
                if (Long.compare(lastId, sequenceCurrentValue) > 0) {
                    executeAlterSequenceValue(lastId + 1);
                }
            }
        }
    }

    private void executeAlterSequenceValue(long newValue) {
        try {
            executeSqlStatement(getAlterSequenceValueSQL(newValue));
        } catch (SQLException e) {
            throw new RepositoryException("Error setting the sequence value to maximum id value using SQL: " + SET_SEQUENCE_VALUE_TO_MAX_ID_SQL, e);
        }
    }


    private void executeSequenceValueToMaxId() {
        try {
            executeSqlStatement(SET_SEQUENCE_VALUE_TO_MAX_ID_SQL);
        } catch (SQLException e) {
            throw new RepositoryException("Error setting the sequence value to maximum id value using SQL: " + SET_SEQUENCE_VALUE_TO_MAX_ID_SQL, e);
        }
    }


    private Long getSequenceCurrentValue() {

        Long currentValue;

        try (Connection conn = dataSource.getConnection();
             Statement statement = conn.createStatement()
        ) {
            ResultSet rs = statement.executeQuery(getSequenceLastValueSQL());
            if (rs.next()) {
                currentValue = rs.getLong(1);
            } else {
                throw new RepositoryException("Unable to obtain the sequence current value using SQL: " + getSequenceLastValueSQL());
            }

        } catch (SQLException e) {
            throw new RepositoryException("Error obtaining the sequence current value using SQL: " + getSequenceLastValueSQL(), e);
        }

        return currentValue;
    }

    void executeSqlStatement(String sql) throws SQLException {
        try (Connection conn = dataSource.getConnection();
             Statement statement = conn.createStatement()
        ) {
            statement.execute(sql);
            log.debug("Successfully Executed SQL Statement " + sql);
        }
    }


    private <S extends T> boolean isNew(S entity) {
        boolean newEntity = isNewEntity(entity);

        if (newEntity) {
            if (entity instanceof EntityWithEmcId) {
                // this is possibly a new entity since the local id indicates this i.e. is <=  0.
                // but need to check the emc id.  If this is > 0 then this entity is being managed by RMC.
                // if we already have a record of it in the emcIdToIdCache then is is not new.

                EntityWithEmcId entityWithEmcId = (EntityWithEmcId) entity;

                if (entityWithEmcId.getEmcId() > 0) {
                    // this is definitely an entity being managed by RMC if exists in the map then is not new otherwise it is new and we have just received it from RMC.
                    synchronized (cache) {
                        Long id = (Long) emcIdToIdCache.get(entityWithEmcId.getEmcId());
                        newEntity = (id == null);
                        if (!newEntity) entityWithEmcId.setId(id);
                    }
                }
            }
        } else {
            // we think this is an existing entity (not newEntity) but if it does not already exist in the cache it is not.
            synchronized (cache) {
                newEntity = (cache.get(getId(entity)) == null);
            }
        }

        return newEntity;
    }


    private <S extends T> S insertIdAutoGenerated(S entity) {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(INSERT_AUTO_ID_SQL, Statement.RETURN_GENERATED_KEYS)
            ) {
                setParameterValues(ps, entity, SqlOperation.INSERT, true, false);

                int affectedRows = ps.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException(String.format("Insert of %s failed, no rows affected.", entity));
                }

                try (ResultSet generatedKeys = ps.getGeneratedKeys()) {
                    if (generatedKeys.next()) {
                        setId(entity, generatedKeys);
                    } else {
                        throw new SQLException(String.format("Insert of %s failed, no ID obtained.", entity));
                    }
                }
            }
        } catch (Exception e) {
            throw new RepositoryException(String.format("Error inserting entity %s, using SQL %s", entity, INSERT_AUTO_ID_SQL), e);
        }


        log.debug("Entity inserted into repository " + entity);
        return newEntity(entity);
    }


    private <S extends T> S insertIdSupplied(S entity) {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(INSERT_ID_SUPPLIED_SQL)
            ) {
                setParameterValues(ps, entity, SqlOperation.INSERT, false, false);

                int affectedRows = ps.executeUpdate();

                if (affectedRows == 0) {
                    throw new SQLException(String.format("Insert of %s failed, no rows affected.", entity));
                }
            }
        } catch (Exception e) {
            throw new RepositoryException(String.format("Error inserting entity %s, using SQL %s", entity, INSERT_ID_SUPPLIED_SQL), e);
        }

        log.debug("Entity inserted into repository " + entity);
        return newEntity(entity);
    }


    private <S extends T> S update(S entity, boolean incrementVersion) {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(UPDATE_SQL)
            ) {
                setParameterValues(ps, entity, SqlOperation.UPDATE, false, incrementVersion);
                ps.executeUpdate();
            }
        } catch (Exception e) {
            throw new RepositoryException(String.format("Error updating entity %s, using SQL %s", entity, UPDATE_SQL), e);
        }

        log.debug("Entity updated in repository " + entity);

        if (incrementVersion) {
            incrementVersion(entity);
        }

        return newEntity(entity);
    }


    /**
     * This should be overridden if versioning is being performed in order to increment the version of the entity before it is
     * written into the cache.
     *
     * @param entity
     * @param <S>
     */
    protected <S extends T> void incrementVersion(S entity) {
    }

    /**
     * This should be overridden if versioning is supported by the table/entity.
     *
     * @return
     */
    protected boolean supportsVersioning() {
        return false;
    }


    @Override
    public <S extends T> Iterable<S> save(Iterable<S> entities) {
        Collection<S> updatedEntities = new ArrayList();

        for (S entity : entities) {
            updatedEntities.add(save(entity));
        }

        return updatedEntities;
    }

    @Override
    public Optional<T> findOne(ID id) {
        synchronized (cache) {
            T entity = getCachedEntity(id);
            return (entity != null) ? Optional.of(newEntity(entity)) : Optional.empty();
        }
    }


    private Collection<T> getAllEntitiesFromCache() {
        return cache.values().stream().map(entry -> entry.get()).collect(Collectors.toSet());
    }


    @Override
    public Iterable<T> findAll() {
        Collection<T> entities = new ArrayList<T>();

        synchronized (cache) {
            for (T entity : getAllEntitiesFromCache()) {
                entities.add(newEntity(entity));
            }
        }

        return entities;
    }


    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        Collection<T> entities = new ArrayList<>();

        synchronized (cache) {
            for (ID id : ids) {
                T entity = getCachedEntity(id);
                if (entity != null) entities.add(newEntity(getCachedEntity(id)));
            }
        }

        return entities;
    }


    @Override
    public long count() {
        synchronized (cache) {
            return cache.size();
        }
    }

    @Override
    public boolean exists(ID id) {
        synchronized (cache) {
            return cache.containsKey(id);
        }
    }

    @Override
    public void deleteById(ID id) {
        synchronized (cache) {
            delete(getCachedEntity(id));
        }
    }


    @Override
    public ID getIdFromEmcId(long emcId) {
        synchronized (cache) {
            return emcIdToIdCache.get(emcId);
        }
    }


    private void deleteFromRepository(T entity) {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(DELETE_SQL)
            ) {
                setParameterValues(ps, entity, SqlOperation.DELETE, false, false);
                ps.executeUpdate();
                log.debug("Entity deleted from repository " + entity);
            }
        } catch (Exception e) {
            throw new RepositoryException(String.format("Error deleting entity %s, using SQL %s", entity, DELETE_SQL), e);
        }
    }


    @Override
    public <S extends T> S delete(T entity) {
        synchronized (cache) {
            if (entity == null) return null;
            // deleteById from repo
            if (entity instanceof EntityWithEmcId) ((EntityWithEmcId) entity).setId((Long) getId(entity));
            deleteFromRepository(entity);
            // deleteById from cache
            cache.remove(getId(entity));
            if (entity instanceof EntityWithEmcId) emcIdToIdCache.remove(((EntityWithEmcId) entity).getEmcId());
            log.debug("Entity removed from cache " + entity);
            return (S) entity;
        }
    }

    @Override
    public Collection<? extends T> delete(Iterable<? extends T> entities) {
        for (T entity : entities) {
            delete(entity);
        }
        return (Collection<? extends T>) entities;
    }

    @Override
    public void deleteAll() {
        synchronized (cache) {
            deleteAllFromRepository();
            cache.clear();
            emcIdToIdCache.clear();
        }
    }


    private void deleteAllFromRepository() {
        try {
            try (Connection conn = dataSource.getConnection();
                 PreparedStatement ps = conn.prepareStatement(DELETE_ALL_SQL)
            ) {
                ps.executeUpdate();
                log.debug("All entities deleted from repository using SQL " + DELETE_ALL_SQL);
            }
        } catch (SQLException e) {
            throw new RepositoryException(String.format("Error deleting all entities, using SQL %s", DELETE_ALL_SQL), e);
        }
    }


    protected Collection<T> getAllEntities() {
        return getAllEntitiesFromCache();
    }


    @Override
    protected T updateUsingJsonObject(JSONObject jsonObject) throws Exception {
        T entity = createEntityFromJson(jsonObject);
        save(entity);
        return entity;
    }


    @Override
    protected <S extends T> S DeleteUsingJsonObject(JSONObject jsonObject) throws Exception {
        T entity = createEntityFromJson(jsonObject);
        delete(entity);
        return (S) entity;
    }

    private static class DefaultCacheEntry<T> implements CacheEntry<T> {
        private T entity;

        public DefaultCacheEntry(T entity) {
            this.entity = entity;
        }

        @Override
        public T get() {
            return entity;
        }

        @Override
        public void set(T entity) {
            this.entity = entity;
        }
    }


}
