package net.rebasoft.data;

import com.hazelcast.core.IMap;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Abstract class to provide standard Service layer methods.
 * <p>
 * Created by chriswhiteley on 08/11/2016.
 */
public abstract class AbstractService<T, ID extends Serializable> extends AbstractServiceJson<T, ID> {

    private static final Logger log = Logger.getLogger("AbstractService");

   public AbstractService() {
    }

    public AbstractService(RepositoryJsonRUD<T, ID> repository, IMap<ID, T> haMap, boolean serverInStandbyMode) {
        super(repository);
    }

    public void setRepository(RepositoryJsonRUD<T, ID> repository) {
        this.repository = repository;
    }


    protected RepositoryJsonRUD<T, ID> getRepository() {
        return (RepositoryJsonRUD<T, ID>) repository;
    }

    public Optional<T> findOne(ID id) {
        return getRepository().findOne(id);
    }

    public Iterable<T> findAll() {
        return getRepository().findAll();
    }

    public Iterable<T> findAll(Iterable<ID> ids) {
        return getRepository().findAll(ids);
    }

    public long count() {
        return getRepository().count();
    }

    public boolean exists(ID id) {
        return getRepository().exists(id);
    }

    public <S extends T> S save(S entity) {
        // save to the repository
        S updated = getRepository().save(entity);

        return updated;
    }

    public <S extends T> Iterable<S> save(Iterable<S> entities) {
        // save to the repository
        Iterable<S> updated = getRepository().save(entities);

        return updated;
    }


    public void delete(ID id) {
        getRepository().deleteById(id);
    }


    public void delete(T entity) {
        getRepository().delete(entity);
    }


    public void delete(Iterable<? extends T> entities) {
        getRepository().delete(entities);
    }

    public void deleteAll() {
        getRepository().deleteAll();
    }


    @Override
    protected T getValueFromJson(JSONObject jsonObject) throws Exception {
        return repository.createEntityFromJson(jsonObject);
    }

    protected abstract ID getId(T entity);



    protected void refreshCache() {
        if (repository instanceof Cached) {
            ((Cached) repository).refresh();
        }
    }

    public ID getIdFromEmcId(long emcId) {
        if (repository instanceof RepositoryWithEmcId) {
            return ((RepositoryWithEmcId<ID>) repository).getIdFromEmcId(emcId);
        } else {
            return null;
        }
    }

    public void resetAllEmcIds() {
        List<T> updated = new ArrayList<>();

        for (T entity : findAll()) {
            if (entity instanceof EntityWithEmcId) {
                ((EntityWithEmcId) entity).setEmcId(-1);
                updated.add(entity);
            }
        }

        if (!updated.isEmpty()) {
            save(updated);
            // refresh the cache since part of the cache is built on the emcId to id mapping.
            refreshCache();
        }

    }


}
