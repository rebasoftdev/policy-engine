package net.rebasoft.data;


/**
 * Represents an entry in the cache.
 *
 * Created by chriswhiteley on 18/04/2017.
 *
 * @param <T> The entity type.
 */
public interface CacheEntry <T> {
  T get();
  void set(T entity);
}
