package net.rebasoft.data;

/**
 * Created by chriswhiteley on 25/11/2016.
 */
public interface Cached {
    void refresh();
}
