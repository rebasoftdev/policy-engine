package net.rebasoft.data;


import java.io.Serializable;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Interface to provide template methods for accessing a repository representing a tree hierarchy.
 * <p>
 * Created by chriswhiteley on 09/03/2017.
 */
public interface RepositoryTree<T, ID extends Serializable> extends Repository<T, ID> {

    void removeChild(T child);

    void removeChild(ID childId);

    /**
     * This method clears all the hierachical information.
     */
    void removeAllChildren();

    // todo: note need to detect loops
    void addChild(T parent, T child);

    void addChild(ID parentId, ID childId);

    Optional<T> getParent(T entity);

    Stream<T> getChildren(T parent);

    Stream<T> getDescendants(T parent);

    Stream<T> getAncestors(T entity);

    boolean isRoot(T entity);

    boolean isChild(T entity);

    boolean isLeaf(T entity);

}
