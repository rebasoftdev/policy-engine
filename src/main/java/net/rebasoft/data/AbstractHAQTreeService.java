package net.rebasoft.data;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import net.rebasoft.data.ha.HAServer;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * This class is same as AbstractHATreeService except that it uses the Hazelcast queue method for doing HA.
 * <p>
 * Abstract class to provide standard Service layer methods along with Hiararchical tree Services. Note for implementation of HA (PeerUpdater interface) it is
 * using the standard Hazelcast IMap approach that has traditionally been used.  If another approach is required these
 * methods should be overridden or a new class (implementation) created.
 * Note for HA of hierarchy it relies on a separate HA map structure containing parent ID, child ID.
 * <p>
 * Created by chriswhiteley on 20/04/2017.
 *
 * @param <T>
 * @param <ID>
 */
public abstract class AbstractHAQTreeService<T, ID extends Serializable & Comparable<ID>> extends AbstractHAQService<T, ID> implements HATreeService<T, ID> {


    private static final Logger log = Logger.getLogger("AbstractHAQTreeService");


    private Collection<IQueue<DataUpdate<TreeEdge<ID>>>> peerHierUpdateQueues;


    public AbstractHAQTreeService(AbstractCachedSQLTreeRepository<T, ID> repository, HazelcastInstance hazelcastInstance, HAServer haServer) {
        super(repository, hazelcastInstance, haServer);
    }


    private IQueue<DataUpdate<TreeEdge<ID>>> getUpdateQueue(String peerId) {
        String updateQueueName = repository.getTypeClass().getSimpleName() + "_update_heir_queue_" + peerId;
        return hazelcastInstance.getQueue(updateQueueName);
    }

    @Override
    void startUpHAQueues(HAServer haServer) {
        // set up HA hierarchical queues for each peer server for this server to send its updates to
        peerHierUpdateQueues = haServer.getPeerIds().stream().map(peerId -> getUpdateQueue(peerId)).collect(Collectors.toSet());
//        super.startUpHAQueues(haServer);

        // start thread to handle data updates to this server from other peer servers in HA cluster
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new QueueHierHandler(getUpdateQueue(haServer.getId())));
    }

    @Override
    void setUpNoQueuing() {
        super.setUpNoQueuing();
        // set up empty queuing our changes are going nowhere!
        peerHierUpdateQueues = new ArrayList();
    }

    public void setRepository(AbstractCachedSQLTreeRepository<T, ID> repository) {
        this.repository = repository;
    }

    @Override
    protected AbstractCachedSQLTreeRepository<T, ID> getRepository() {
        return (AbstractCachedSQLTreeRepository<T, ID>) repository;
    }

    @Override
    public synchronized void sync() {
        super.sync();
        hierarchicalSync();
    }

    private void updatePeers(ID parent, ID child, CUD operation) {
        try {
            for (IQueue<DataUpdate<TreeEdge<ID>>> queue : peerHierUpdateQueues) {
                DataUpdate<TreeEdge<ID>> update = new DataUpdate(new TreeEdge<ID>(parent, child), operation, TreeEdge.class);
                log.debug(String.format("Adding hierarchical update %s to peer queue %s", update, queue.getName()));
                queue.put(update);
            }
        } catch (InterruptedException e) {
            log.info("updatePeers was interrupted.");
        }
    }

    protected void updatePeers(T entity, CUD operation) {

        try {
            for (IQueue queue : peerHierUpdateQueues) {
                DataUpdate<T> update = new DataUpdate(entity, operation, getRepository().getTypeClass());
                log.debug(String.format("Adding update %s to peer queue %s", update, queue.getName()));
                queue.put(update);
            }
        } catch (InterruptedException e) {
            log.info("updatePeers was interrupted.");
        }
    }

    private void updatePeers(CUD operation) {
        try {
            for (IQueue queue : peerHierUpdateQueues) {
                DataUpdate<TreeEdge<ID>> update = new DataUpdate(operation, TreeEdge.class);
                log.debug(String.format("Adding update %s to peer queue %s", update, queue.getName()));
                queue.put(update);
            }
        } catch (InterruptedException e) {
            log.info("updatePeers was interrupted.");
        }
    }


    private synchronized void hierarchicalSync() {
        log.debug("Syncing HA peers with hierarchy data in this repo.");

        // just do this simply by first telling peers to remove all hierarchy (all children);

        updatePeers(CUD.DELETE_ALL);

        // now go through each entity and if it has a parent send a CREATE child (parent id, child id) to all peers
        getRepository().findAll().forEach(
                entity -> getRepository().getParent(entity).ifPresent(parent -> updatePeers(getId(parent), getId(entity), CUD.CREATE)));

        log.debug("Syncing HA peers with hierarchy data in repo completed.");
    }

    public void removeChild(T child) {
        Optional<T> parentOptional = getRepository().getParent(child);
        parentOptional.ifPresent(parent -> {
                    getRepository().removeChild(child);
                    updatePeers(getId(parent), getId(child), CUD.DELETE);
                }
        );
    }


    public void removeChild(ID childId) {
        getRepository().findOne(childId).ifPresent(child -> removeChild(child));
    }

    /**
     * This method clears all the hierarchical information.
     */
    public void removeAllChildren() {
        getRepository().removeAllChildren();
        updatePeers(CUD.DELETE_ALL);
    }


    public void addChild(T parent, T child) {
        getParent(child).ifPresent(currentParent -> {
            if (!currentParent.equals(parent)) removeChild(child);
        });

        getRepository().addChild(parent, child);
        updatePeers(getId(parent), getId(child), CUD.CREATE);
    }

    public void addChild(ID parentId, ID childId) {
        addChild(getRepository().findOne(parentId).get(), getRepository().findOne(childId).get());
    }

    public Optional<T> getParent(T entity) {
        return getRepository().getParent(entity);
    }

    public Stream<T> getChildren(T parent) {
        return getRepository().getChildren(parent);
    }

    public Stream<T> getDescendants(T parent) {
        return getRepository().getDescendants(parent);
    }

    public Stream<T> getAncestors(T entity) {
        return getRepository().getAncestors(entity);
    }

    public boolean isRoot(T entity) {
        return getRepository().isRoot(entity);
    }

    public boolean isChild(T entity) {
        return getRepository().isChild(entity);
    }

    public boolean isLeaf(T entity) {
        return getRepository().isLeaf(entity);
    }


    private void processHierachicalDataUpdate(DataUpdate<TreeEdge<ID>> dataUpdate) {
        TreeEdge<ID> receivedEdge = dataUpdate.getData();

        synchronized (this) {
            switch (dataUpdate.getOperation()) {
                case CREATE:
                case UPDATE:
                    getRepository().addChild(receivedEdge.parentId, receivedEdge.childId);
                    log.debug(String.format("HA Queue Hierarchical data handler, child added, parent id [%s], child id [%s].", receivedEdge.parentId, receivedEdge.childId));
                    break;
                case DELETE:
                    getRepository().removeChild(receivedEdge.childId);
                    log.debug(String.format("HA Queue Hierarchical data handler, child removed, parent id [%s], child id [%s].", receivedEdge.parentId, receivedEdge.childId));
                    break;
            }
        }
    }

    /**
     * The TreeEdge class represents the connection between one node in a tree and another
     *
     * @param <ID>
     */
    private static class TreeEdge<ID extends Serializable> implements Serializable {
        private ID parentId;
        private ID childId;

        public TreeEdge(ID parentId, ID childId) {
            this.parentId = parentId;
            this.childId = childId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TreeEdge)) return false;

            TreeEdge<?> treeEdge = (TreeEdge<?>) o;

            return parentId.equals(treeEdge.parentId) && childId.equals(treeEdge.childId);
        }

        @Override
        public int hashCode() {
            int result = parentId.hashCode();
            result = 31 * result + childId.hashCode();
            return result;
        }

        @Override
        public String toString() {
            return "TreeEdge{" +
                    "parentId=" + parentId +
                    ", childId=" + childId +
                    '}';
        }
    }


    private class QueueHierHandler implements Runnable {

        private IQueue updateQueue;

        public QueueHierHandler(IQueue updateQueue) {
            this.updateQueue = updateQueue;
        }


        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    DataUpdate dataUpdate = (DataUpdate) updateQueue.take();
                    synchronized (this) {
                        if (dataUpdate.getType().equals(TreeEdge.class)) processHierachicalDataUpdate(dataUpdate);
                        else processUpdate(dataUpdate);
                    }
                } catch (InterruptedException ie) {
                    log.info(String.format("HA Hierarchical Queue handler thread for entity type [%s] interrupted, exiting thread", getType()));
                    Thread.currentThread().interrupt();
                } catch (IllegalStateException is) {
                    log.info(String.format("HA Hierarchical Queue handler thread for entity type [%s] received IllegalStateException, exiting thread", getType()), is);
                    Thread.currentThread().interrupt();
                } catch (Throwable t) {
                    log.info(String.format("HA Hierarchical Queue handler thread for entity type [%s] received exception, continuing...", getType()), t);
                }
            }

            log.info("QueueHierHandler thread exiting");
        }

    }
}
