package net.rebasoft.data;

import java.util.*;
import java.util.stream.Stream;

/**
 * Created by chriswhiteley on 08/02/2017.
 */
public class TreeNode<T> implements  Iterable<TreeNode<T>>{

    private T data;
    private TreeNode<T> parent;
    private final Set<TreeNode<T>> children;

    public TreeNode(T data) {
        this.data = data;
        this.children = new LinkedHashSet<>();
    }

    public TreeNode<T> addChildNode(TreeNode<T> childNode) {
        childNode.parent = this;
        children.add(childNode);
        return childNode;
    }


    public TreeNode<T> removeChildNode(TreeNode<T> childNode) {
        childNode.parent = null;
        children.remove(childNode);
        return childNode;
    }

    public void removeChildren() {
        children.forEach(childNode -> childNode.parent = null);
        children.clear();
    }



    public TreeNode<T> addChild(T child) {
        TreeNode<T> childNode = new TreeNode<T>(child);
        childNode.parent = this;
        children.add(childNode);
        return childNode;
    }

    public boolean isRoot() {
        return parent == null;
    }

    public boolean isChild() {
        return parent != null;
    }
    public boolean isLeaf() {
        return children.size() == 0;
    }

    public int getLevel() {
        if (this.isRoot())
            return 0;
        else
            return parent.getLevel() + 1;
    }

    public TreeNode<T> getParent() {
        return parent;
    }

    public T getData() {return data;}

    public void setData(T data) { this.data = data;}

    public Collection<TreeNode<T>> getChildren() {
        return Collections.unmodifiableSet(children);
    }


    /**
     * get all the ancestors of this node.
     *
     * @return
     */
    public Stream<TreeNode<T>> ancestors() {
        if (parent==null) return Stream.empty();
        return Stream.concat(Stream.of(parent),parent.ancestors());
    }


    /**
     * Get all the descendants of this node.
     *
     * @return
     */
    public Stream<TreeNode<T>> descendants() {
        return Stream.concat(children.stream(), children.stream().flatMap(TreeNode::descendants));
    }


    @Override
    public String toString() {
        return data != null ? data.toString() : "[data null]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TreeNode)) return false;

        TreeNode<?> treeNode = (TreeNode<?>) o;

        return getData().equals(treeNode.getData());
    }

    @Override
    public int hashCode() {
        return getData().hashCode();
    }

    @Override
    public Iterator<TreeNode<T>> iterator() {
        return new TreeNodeIterator<>(this);
    }

    /**
     * Clears the node of hierarchy data i.e. removes its links to other nodes by clearing its parent and children.
     * Note. it does not clear the data.
     */
    public void clear() {
        parent = null;
        children.clear();
    }

    private static class TreeNodeIterator<T> implements Iterator<TreeNode<T>> {

        enum ProcessStages {
            ProcessParent, ProcessChildCurNode, ProcessChildSubNode
        }

        private final TreeNode<T> treeNode;

        public TreeNodeIterator(TreeNode<T> treeNode) {
            this.treeNode = treeNode;
            this.doNext = ProcessStages.ProcessParent;
            this.childrenCurNodeIter = treeNode.children.iterator();
        }

        private ProcessStages doNext;
        private TreeNode<T> next;
        private final Iterator<TreeNode<T>> childrenCurNodeIter;
        private Iterator<TreeNode<T>> childrenSubNodeIter;

        @Override
        public boolean hasNext() {

            if (this.doNext == ProcessStages.ProcessParent) {
                this.next = this.treeNode;
                this.doNext = ProcessStages.ProcessChildCurNode;
                return true;
            }

            if (this.doNext == ProcessStages.ProcessChildCurNode) {
                if (childrenCurNodeIter.hasNext()) {
                    TreeNode<T> childDirect = childrenCurNodeIter.next();
                    childrenSubNodeIter = childDirect.iterator();
                    this.doNext = ProcessStages.ProcessChildSubNode;
                    return hasNext();
                }

                else {
                    this.doNext = null;
                    return false;
                }
            }

            if (this.doNext == ProcessStages.ProcessChildSubNode) {
                if (childrenSubNodeIter.hasNext()) {
                    this.next = childrenSubNodeIter.next();
                    return true;
                }
                else {
                    this.next = null;
                    this.doNext = ProcessStages.ProcessChildCurNode;
                    return hasNext();
                }
            }

            return false;
        }

        @Override
        public TreeNode<T> next() {
            return this.next;
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }



}
