package net.rebasoft.data;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Collection;

/**
 * Abstract class to provide standard Service layer methods for handling JSON CRUD operations.
 * <p>
 * Created by chriswhiteley on 31/10/2016.
 */
public abstract class AbstractServiceJson<T, ID extends Serializable> implements ServiceJson<T, ID> {
    private static final Logger log = Logger.getLogger("AbstractServiceJson");

    protected RepositoryJson<T, ID> repository;

    public AbstractServiceJson() {
    }

    public AbstractServiceJson(RepositoryJson<T, ID> repository) {
        this.repository = repository;
    }

    @Override
    public void setRepository(RepositoryJson<T, ID> repository) {
        this.repository = repository;
    }


    /**
     * This method allows overriding if JSON string is different from RMC e.g. need to convert emcid to id.
     *
     * @param jsonString
     * @throws Exception
     */
    @Override
    public void updateFromRmcJson(String jsonString) throws Exception {
        updateFromJson(jsonString);
    }

    /**
     * Uses the jsonString which contains a JSON array of JSON objects each of which maps to an entity of type T.
     * For each entity it is saved to the repository by updating the entity in the repository if it already exists or
     * creating the entity in the repository if it does not exist.
     *
     * @param jsonString
     * @throws Exception
     */
    @Override
    public void updateFromJson(String jsonString) throws Exception {
        // update the repository
        Collection<? extends T> updates = repository.updateFromJson(jsonString);
    }

    protected abstract T getValueFromJson(JSONObject jsonObject) throws Exception;

    protected JSONArray getJsonArray(String jsonString) throws JSONException {
        return repository.getJsonArray(jsonString);
    }


    @Override
    public String getAllAsJson() throws Exception {
        return repository.getAllAsJson();
    }


    @Override
    public void deleteFromRmcJson(String jsonString) throws Exception {
        deleteFromJson(jsonString);
    }


    /**
     * This method allows overriding if JSON string is different from RMC e.g. need to convert emcid to id.
     *
     * @param jsonString
     * @throws Exception
     */
    @Override
    public void deleteFromJson(String jsonString) throws Exception {
        // update the repository
        Collection<? extends T> deletes = repository.deleteFromJson(jsonString);

    }

}
