package net.rebasoft.data;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Collection;

/**
 * Abstract class to provide standard Service layer methods for handling JSON CRUD operations.
 * <p>
 * Created by chriswhiteley on 31/10/2016.
 */
public abstract class AbstractHAServiceJson<T, ID extends Serializable> extends AbstractServiceJson<T, ID> implements HAServiceJson<T, ID> {
    private static final Logger log = Logger.getLogger("AbstractServiceJsonWithHA");

    protected RepositoryJson<T, ID> repository;

    public AbstractHAServiceJson() {
    }

    public AbstractHAServiceJson(RepositoryJson<T, ID> repository) {
        this.repository = repository;
    }

    public void setRepository(RepositoryJson<T, ID> repository) {
        this.repository = repository;
    }


    /**
     * This method allows overriding if JSON string is different from RMC e.g. need to convert emcid to id.
     *
     * @param jsonString
     * @throws Exception
     */
    public void updateFromRmcJson(String jsonString) throws Exception {
        updateFromJson(jsonString);
    }

    public void updateFromJson(String jsonString) throws Exception {
        // update the repository
        Collection<? extends T> updates = repository.updateFromJson(jsonString);

        // update HA Peers
        updates.forEach(this::sendUpdateToPeers);
    }

    protected abstract T getValueFromJson(JSONObject jsonObject) throws Exception;

    protected JSONArray getJsonArray(String jsonString) throws JSONException {
        return repository.getJsonArray(jsonString);
    }


    public String getAllAsJson() throws Exception {
        return repository.getAllAsJson();
    }


    public void deleteFromRmcJson(String jsonString) throws Exception {
        deleteFromJson(jsonString);
    }


    /**
     * This method allows overriding if JSON string is different from RMC e.g. need to convert emcid to id.
     *
     * @param jsonString
     * @throws Exception
     */
    public void deleteFromJson(String jsonString) throws Exception {
        // update the repository
        Collection<? extends T> deletes = repository.deleteFromJson(jsonString);

        // update HA Peers
        deletes.forEach(this::sendDeleteToPeers);
    }

}
