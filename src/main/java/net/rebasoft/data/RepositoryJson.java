package net.rebasoft.data;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Collection;

/**
 * Represents a repository for basic operations against the database using JSON strings.
 */
public interface RepositoryJson<T, ID extends Serializable> extends Repository<T, ID> {
    Logger log = Logger.getLogger("RepositoryJson");


    /**
     * Queries the repository to obtain all rows and returns the results as a JSON string containing an array of the
     * resultant records (as JSON objects).
     *
     * @return
     * @throws Exception
     */
    String getAllAsJson() throws Exception;

    /**
     * Updates the repository with the data in the JSON string.
     * Note that this should update if the record exists in the repository or create the record if it does not.
     *
     * @param jsonString A JSON string containing one or more records (as JSON objects) in a JSON array.
     * @throws JSONException
     * @returns
     */
    Collection<? extends T> updateFromJson(String jsonString);


    /**
     * Deletes rows from the repository using the data in the JSON string.
     *
     * @param jsonString A JSON string containing one or more records (as JSON objects) in a JSON array.
     * @throws JSONException
     * @returns the deleted entities.
     */
    Collection<? extends T> deleteFromJson(String jsonString);


    /**
     * Converts the JSONObject to the entity type.
     *
     * @param jsonObject
     * @return
     * @throws Exception
     */
    T createEntityFromJson(JSONObject jsonObject) throws Exception;


    default JSONArray getJsonArray(String jsonString) {
        JSONArray jsonArray = null;
        try {
            jsonArray = new JSONArray(jsonString);
        } catch (JSONException e) {
            log.error(String.format("Error converting JSON string into JSONArray. JSON string: %s", jsonString), e);
            throw new RepositoryException(String.format("Error converting JSON string into JSONArray. JSON string: %s", jsonString), e);
        }
        return jsonArray;
    }


}
