package net.rebasoft.data;

import java.io.Serializable;

/**
 * Created by chriswhiteley on 08/11/2016.
 */
public interface RepositoryJsonRUD <T,ID extends Serializable> extends RepositoryJson<T,ID>, RepositoryR<T,ID>,RepositoryU<T,ID>, RepositoryD<T,ID>, RepositoryUVersion<T, ID> {
}
