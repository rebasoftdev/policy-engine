package net.rebasoft.data;

import com.hazelcast.core.IMap;
import com.hazelcast.core.ISet;
import com.hazelcast.core.ItemEvent;
import com.hazelcast.core.ItemListener;
import net.rebasoft.data.ha.HAServer;
import net.rebasoft.data.ha.ServerStatus;
import org.apache.log4j.Logger;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * Abstract class to provide standard Service layer methods along with Hiararchical tree Services. Note for implementation of HA (PeerUpdater interface) it is
 * using the standard Hazelcast IMap approach that has traditionally been used.  If another approach is required these
 * methods should be overridden or a new class (implementation) created.
 * Note for HA of hierarchy it relies on a separate HA map structure containing parent ID, child ID.
 * <p>
 * Created by chriswhiteley on 20/04/2017.
 *
 * @param <T>
 * @param <ID>
 */
public abstract class AbstractHATreeService<T, ID extends Serializable & Comparable<ID>> extends AbstractHAService<T, ID> implements HATreeService<T,ID> {


    private static final Logger log = Logger.getLogger("AbstractTreeServiceWithHA");

    private ISet<TreeEdge<ID>> haHierarchySet;
    // localHierarchyList is used to backup the Hazelcast distributed ISet (haHierarchySet) in order to provide faster access, it is maintained with the same data.
    private List<TreeEdge<ID>> localHierarchyList;
    private String haHierachicalItemListenerId;

    public AbstractHATreeService() {
        localHierarchyList = new ArrayList();
    }

    public AbstractHATreeService(AbstractCachedSQLTreeRepository<T, ID> repository, IMap<ID, T> haMap, HAServer haServer) {
        super(repository, haMap, haServer);
        localHierarchyList = new ArrayList();
    }

    public void setRepository(AbstractCachedSQLTreeRepository<T, ID> repository) {
        this.repository = repository;
    }

    protected AbstractCachedSQLTreeRepository<T, ID> getRepository() {
        return (AbstractCachedSQLTreeRepository<T, ID>) repository;
    }

    public void setHaHierarchy(ISet<TreeEdge<ID>> haHierarchySet) {
        this.haHierarchySet = haHierarchySet;
    }

    @Override
    public synchronized void sync() {
        super.sync();
        hierarchicalSync();
    }

    @Override
    synchronized void syncRepoToHaMap() {
        super.syncRepoToHaMap();
        syncHierarchicalRepoFromHaData();
    }

    private void syncHierarchicalRepoFromHaData() {
        // clear the hierarchy from the repository
        getRepository().removeAllChildren();

        localHierarchyList.clear();
        localHierarchyList.addAll(haHierarchySet);

        // go through the HA hierarchy data and set things back up!
        localHierarchyList.forEach(edge -> getRepository().addChild(edge.parentId, edge.childId));
    }

    @Override
    protected void addHaListeners() {
        super.addHaListeners();
        addHaHierarchicalListeners();
    }

    @Override
    protected void removeHaListeners() {
        super.removeHaListeners();
        removeHaHierachicalListeners();
    }

    private void addHaHierarchicalListeners() {
        haHierachicalItemListenerId = haHierarchySet.addItemListener(new ItemListener<TreeEdge<ID>>() {
            @Override
            public void itemAdded(ItemEvent<TreeEdge<ID>> itemEvent) {
                if (haServer.getStatus() == ServerStatus.STANDBY_WITH_MASTER) {
                    TreeEdge<ID> edge = itemEvent.getItem();
                    addToLocalList(edge);
                    getRepository().addChild(edge.parentId, edge.childId);
                    log.debug(String.format("HA hierarchy listener itemAdded, new child added, parent id [%s], child id [%s].", edge.parentId, edge.childId));
                }
            }

            @Override
            public void itemRemoved(ItemEvent<TreeEdge<ID>> itemEvent) {
                if (haServer.getStatus() == ServerStatus.STANDBY_WITH_MASTER) {
                    TreeEdge<ID> edge = itemEvent.getItem();
                    localHierarchyList.remove(edge);
                    getRepository().removeChild(edge.childId);
                    log.debug(String.format("HA hierarchy listener itemRemoved, child removed, parent id [%s], child id [%s].", edge.parentId, edge.childId));
                }

            }
        }, true);

    }

    private void addToLocalList(TreeEdge<ID> edge) {
        int index = localHierarchyList.indexOf(edge);
        if (index >= 0) localHierarchyList.set(index, edge);
        else localHierarchyList.add(edge);
    }


    private void removeHaHierachicalListeners() {
        log.debug("Removing HA Hierarchy listener for entity type " + repository.getTypeClass());
        haHierarchySet.removeItemListener(haHierachicalItemListenerId);
    }

    private synchronized void hierarchicalSync() {
        log.debug("Syncing HA Hierarchy map with repo.");

        // just do this simply by first clearing the map and then setting up the hierarchy.
        haHierarchySet.clear();
        localHierarchyList.clear();

        getRepository().findAll().forEach(entity -> {
            getRepository().getParent(entity).ifPresent(parent -> localHierarchyList.add(new TreeEdge<ID>(getId(parent), getId(entity))));
        });

        // now populate the HA set in one hit.
        haHierarchySet.addAll(localHierarchyList);

        log.debug("Syncing HA Hierarchy map with repo completed.");
    }

    public void removeChild(T child) {
        Optional<T> parentOptional = getRepository().getParent(child);
        parentOptional.ifPresent(parent -> {
                    getRepository().removeChild(child);
                    removeFromHaHierarchy(getId(parent), getId(child));
                }
        );
        getRepository().removeChild(child);
    }

    private void removeFromHaHierarchy(ID parent, ID child) {
        TreeEdge<ID> edge = new TreeEdge<>(parent, child);
        haHierarchySet.remove(edge);
        localHierarchyList.remove(edge);
    }

    public void removeChild(ID childId) {
        getRepository().findOne(childId).ifPresent(child -> removeChild(child));
    }

    /**
     * This method clears all the hierarchical information.
     */
    public void removeAllChildren() {
        getRepository().removeAllChildren();
        haHierarchySet.clear();
        localHierarchyList.clear();
    }


    private void addToHaHierarchy(ID parent, ID child) {
        TreeEdge<ID> edge = new TreeEdge<>(parent, child);
        haHierarchySet.add(edge);
        addToLocalList(edge);
    }

    public void addChild(T parent, T child) {
        addChild(parent, child);
        addToHaHierarchy(getId(parent), getId(child));
    }

    public void addChild(ID parentId, ID childId) {
        addChild(getRepository().findOne(parentId).get(), getRepository().findOne(childId).get());
    }

    public Optional<T> getParent(T entity) {
        return getRepository().getParent(entity);
    }

    public Stream<T> getChildren(T parent) {
        return getRepository().getChildren(parent);
    }

    public Stream<T> getDescendants(T parent) {
        return getRepository().getDescendants(parent);
    }

    public Stream<T> getAncestors(T entity) {
        return getRepository().getAncestors(entity);
    }

    public boolean isRoot(T entity) {
        return getRepository().isRoot(entity);
    }

    public boolean isChild(T entity) {
        return getRepository().isChild(entity);
    }

    public boolean isLeaf(T entity) {
        return getRepository().isLeaf(entity);
    }


    /**
     * When deleting an entity from the HA entity map it also needs to be removed from any parent/child hierarchical
     * relationships in the HA hierarchical data structure.
     *
     * @param entity
     */
    @Override
    public void sendDeleteToPeers(T entity) {
        if (haServer.getStatus().isActiveHAServer()) {
            getHaMap(entity).delete(getId(entity));
            deleteHaHierarchyNodeConnections(getId(entity));
            log.debug("Deleted entity from HA map " + entity);
        }
    }

    /**
     * Remove all connections from haHierarchySet which link to this node.
     *
     * @param nodeId
     */
    private void deleteHaHierarchyNodeConnections(ID nodeId) {
        haHierarchySet.forEach(connection -> {
            if (connection.childId.equals(nodeId) || connection.parentId.equals(nodeId))
                haHierarchySet.remove(connection);
        });
    }


    /**
     * The TreeEdge class represents the connection between one node in a tree and another
     *
     * @param <ID>
     */
    private static class TreeEdge<ID extends Serializable> implements Serializable {
        private ID parentId;
        private ID childId;

        public TreeEdge(ID parentId, ID childId) {
            this.parentId = parentId;
            this.childId = childId;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof TreeEdge)) return false;

            TreeEdge<?> treeEdge = (TreeEdge<?>) o;

            if (!parentId.equals(treeEdge.parentId)) return false;
            return childId.equals(treeEdge.childId);
        }

        @Override
        public int hashCode() {
            int result = parentId.hashCode();
            result = 31 * result + childId.hashCode();
            return result;
        }
    }


}
