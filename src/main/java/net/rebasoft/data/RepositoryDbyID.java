package net.rebasoft.data;

import java.io.Serializable;

/**
 * Represents a Generic repository for Delete by Id operations.
 *
 * @param <ID> The Id (Primary key) type.
 */
public interface RepositoryDbyID <T,ID extends Serializable> extends Repository<T,ID> {


    /**
     * Deletes the entity with the given id.
     *
     * @param id
     */
     void deleteById(ID id);
}
