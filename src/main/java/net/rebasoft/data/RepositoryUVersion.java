package net.rebasoft.data;

import java.io.Serializable;

/**
 * Repository interface that allows more control over versioning when saving.
 *
 * @param <T> The data Type (Entity).
 * @param <ID> The Id (Primary key) type.
 */
public interface RepositoryUVersion<T, ID extends Serializable> extends Repository<T, ID> {

    /**
     * Save a single entity to the repo. This determines whether the entity is a new entity that needs to be created
     * in the repository or an existing entity that needs to be updated in the repository.  If this is a new entity then
     * the method may allow for the ID to be sequence generated.
     *
     * @param entity
     * @param incrementVersion
     * @param <S>
     * @return
     */
    <S extends T> S save(S entity, boolean incrementVersion);


    /**
     * Save a single entity to the repo with no sequence generation of id on create.  This is as per the previous method but
     * can be used where we don't require auto generation of the id using a sequence.
     *
     * @param entity
     * @param incrementVersion
     * @param <S>
     * @return
     */
    <S extends T> S saveNoSequence(S entity, boolean incrementVersion);


}
