package net.rebasoft.data;

/**
 * Created by chriswhiteley on 20/04/2017.
 */
public class ChildAlreadyHasParentException extends RepositoryException {
    public ChildAlreadyHasParentException() {
        super();
    }

    public ChildAlreadyHasParentException(String message) {
        super(message);
    }
}
