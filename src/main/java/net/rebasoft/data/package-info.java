/**
 * Package that contains common database utilities and repository interfaces.
 *
 * Created by chriswhiteley on 02/09/2016.
 */
package net.rebasoft.data;