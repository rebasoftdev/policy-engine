package net.rebasoft.data;

import java.io.Serializable;
import java.util.Optional;

/**
 * Repository interface with standard read methods.
 *
 * @param <T> The data Type (Entity).
 * @param <ID> The Id (Primary key) type.
 */
public interface   RepositoryR <T, ID extends Serializable>  extends Repository<T,ID> {


    /**
     * Retrieves and entity by its id.
     * @param id The id.
     * @return the retrieved entity or null if no entity was found.
     */
    Optional<T> findOne(ID id);


    /**
     * Returns all instances in the repo.
     * @return
     */
    Iterable<T>	findAll();


    /**
     * Returns all instances of the entity with the given IDs.
     *
     * @param ids
     * @return
     */
    Iterable<T>	findAll(Iterable<ID> ids);


    /**
     * Returns the count of entities in the repo.
     * @return
     */
    long count();

    /**
     * Returns true if an entity with this id exists in the repo.
     * @param id
     * @return
     */
    boolean	exists(ID id);


}
