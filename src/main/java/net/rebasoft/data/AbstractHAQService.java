package net.rebasoft.data;

import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IQueue;
import net.rebasoft.data.ha.HAServer;
import net.rebasoft.data.ha.HaSyncer;
import net.rebasoft.data.ha.ServerStatus;
import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * Abstract class to provide standard Service layer methods. Note for implementation of HA (PeerUpdater interface) it is
 * using the Hazelcast IQueue instead of IMap.
 * <p>
 * This works as follows:-
 * - each peer has a queue to listen on for updates (inserts, updates, and deletes) this is named "updateQueue" + peerId
 * listening is performed by a thread.  This is only done for HA servers.
 * - when an update is performed locally then it is also put on the update queue for each of this servers peers.
 * - syncing is available at either end.
 * <p>
 * <p>
 * Created by chriswhiteley on 25/4/2017.
 */
public abstract class AbstractHAQService<T, ID extends Serializable & Comparable<ID>> extends AbstractHAServiceJson<T, ID> implements HAService<T, ID> {

    private static final Logger log = Logger.getLogger("AbstractHAQService");


    private Collection<IQueue<DataUpdate<T>>> peerUpdateQueues;

    protected HAServer haServer;
    HazelcastInstance hazelcastInstance;

    public AbstractHAQService() {
    }

    public AbstractHAQService(RepositoryJsonRUD<T, ID> repository, HazelcastInstance hazelcastInstance, HAServer haServer) {
        super(repository);
        setHa(hazelcastInstance, haServer);
    }

    private IQueue<DataUpdate<T>> getUpdateQueue(String peerId) {
        String updateQueueName = repository.getTypeClass().getSimpleName() + "_update_queue_" + peerId;
        return hazelcastInstance.getQueue(updateQueueName);
    }

    public void setHa(HazelcastInstance hazelcastInstance, HAServer haServer) {
        this.haServer = haServer;
        this.hazelcastInstance = hazelcastInstance;
    }

    protected Class<T> getType() {
        return repository.getTypeClass();
    }


    public void startHa() {
        log.debug("Setting HA Server as " + haServer + " for entity class " + getType());

        // only startup HA if we are a HA server (i.e. not a Stand Alone server)
        if (haServer.getStatus() != ServerStatus.STANDALONE) {
            startUpHAQueues(haServer);
        } else {
            // set up an empty collection of UpdateQueues (our updates are going nowhere)
            setUpNoQueuing();
        }
    }

    void setUpNoQueuing() {
        peerUpdateQueues = new ArrayList();
    }

    void startUpHAQueues(HAServer haServer) {
        // set up HA queues for each peer server for this server to send its updates to
        peerUpdateQueues = haServer.getPeerIds().stream().map(peerId -> getUpdateQueue(peerId)).collect(Collectors.toSet());

        // start thread to handle data updates to this server from other peer servers in HA cluster
        Executor executor = Executors.newSingleThreadExecutor();
        executor.execute(new QueueHandler(getUpdateQueue(haServer.getId())));
        startSync(); // sync peer servers to what this server has
    }


    private void startSync() {
        Runnable syncTask = this::sync;
        new Thread(syncTask).start();
    }


    /**
     * This is called when the state of this server has changed from either ACTIVE to STANDBY or STANDBY to ACTIVE.
     * <p>
     * If this server has just gone ACTIVE this means it could have been a STANDBY server and if it uses a sequence this
     * could be behind the latest id so first this needs to be brought back into alignment.  Then we remove the listeners
     * from the HA map otherwise any changes we make will cause the listeners to fire.
     * <p>
     * If this server has just gone to STANDBY it means it originally was a STANDBY server that went ACTIVE but has now
     * gone back to STANDBY.  If it uses a sequence then this should be in alignment with the last id but we will need
     * to re add the listeners to the HA map so that any changes made by the MASTER server are replicated.
     * *
     *
     * @param status The ServerState this server has just gone to.
     */
    @Override
    public void stateChanged(ServerStatus status) {
        log.debug("Received Server Status change to " + status + " for entity class " + getType());
        if (status != ServerStatus.STANDALONE) sync(); // sync peer server to what this server has
    }


    @Override
    public void refreshCache() {
        if (repository instanceof Cached) {
            ((Cached) repository).refresh();
            startSync();
        }
    }


    public void setRepository(RepositoryJsonRUD<T, ID> repository) {
        this.repository = repository;
    }


    protected RepositoryJsonRUD<T, ID> getRepository() {
        return (RepositoryJsonRUD<T, ID>) repository;
    }

    @Override
    public Optional<T> findOne(ID id) {
        return getRepository().findOne(id);
    }

    @Override
    public Iterable<T> findAll() {
        return getRepository().findAll();
    }

    @Override
    public Iterable<T> findAll(Iterable<ID> ids) {
        return getRepository().findAll(ids);
    }

    @Override
    public long count() {
        return getRepository().count();
    }

    @Override
    public boolean exists(ID id) {
        return getRepository().exists(id);
    }

    @Override
    public <S extends T> S save(S entity) {
        // save to the repository
        S updated = getRepository().save(entity);

        // update HA Peers
        sendUpdateToPeers(updated);
        return updated;
    }

    @Override
    public <S extends T> Iterable<S> save(Iterable<S> entities) {
        // save to the repository
        Iterable<S> updated = getRepository().save(entities);

        // update HA Peers
        for (S s : updated) {
            sendUpdateToPeers(s);
        }

        return updated;
    }


    @Override
    public void deleteById(ID id) {
        Optional<T> entityOptional = getRepository().findOne(id);
        entityOptional.ifPresent(entity -> {
            getRepository().deleteById(id);
            sendDeleteToPeers(entity);
        });
    }


    @Override
    public <S extends T> S delete(T entity) {
        S deleted = getRepository().delete(entity);
        sendDeleteToPeers(entity);
        return deleted;
    }


    @Override
    public Collection<? extends T> delete(Iterable<? extends T> entities) {
        Collection<? extends T> deleted = getRepository().delete(entities);

        for (T entity : entities) {
            sendDeleteToPeers(entity);
        }

        return deleted;
    }

    @Override
    public synchronized void deleteAll() {
        Iterable<T> entities = getRepository().findAll();

        getRepository().deleteAll();

        for (T entity : entities) {
            sendDeleteToPeers(entity);
        }
    }


    @Override
    protected T getValueFromJson(JSONObject jsonObject) throws Exception {
        return repository.createEntityFromJson(jsonObject);
    }

    protected abstract ID getId(T entity);


    protected void updatePeers(T entity, CUD operation) {

        try {
            for (IQueue<DataUpdate<T>> queue : peerUpdateQueues) {
                DataUpdate<T> update = new DataUpdate(entity, operation, getRepository().getTypeClass());
                log.debug(String.format("Adding update [%s] to peer queue [%s] for entity type [%s]", update, queue.getName(), getType()));
                queue.put(update);
            }
        } catch (InterruptedException e) {
            log.info("updatePeers was interrupted.");
        }
    }


    @Override
    public void sendCreateToPeers(T entity) {
        updatePeers(entity, CUD.CREATE);
    }

    @Override
    public void sendUpdateToPeers(T entity) {
        updatePeers(entity, CUD.UPDATE);
    }

    @Override
    public void sendDeleteToPeers(T entity) {
        updatePeers(entity, CUD.DELETE);
    }


    @Override
    public synchronized void sync() {
        log.debug(String.format("Syncing HA with repo for entity type [%s]. Update peers with what we have in our repository.", getType()));
        findAll().forEach(entity -> updatePeers(entity, CUD.UPDATE));
        log.debug(String.format("Syncing HA with repo for entity type [%s] completed.", getType()));
    }


    /**
     * Compares two {@code entity} values to indicate which is the newest (latest).
     * Override this to provide a proper comparison of the two entities (by default it just returns that they are the same version).
     *
     * @param entity1 the first {@code entity} to compare
     * @param entity2 the second {@code entity} to compare
     * @return the value {@code 0} if {@code entity1} is at same version as {@code entity2};
     * a value less than {@code 0} if {@code entity1} is not latest (earlier version) than {@code entity2}
     * a value greater than {@code 0} if {@code entity1} is latest (newer version) than {@code entity2}
     */
    protected int compareLatest(T entity1, T entity2) {
        return 0;
    }

    @Override
    public HaSyncer newSyncer() {
        return this;
    }

    @SuppressWarnings("unused")
    public ID getIdFromEmcId(long emcId) {
        if (repository instanceof RepositoryWithEmcId) {
            return ((RepositoryWithEmcId<ID>) repository).getIdFromEmcId(emcId);
        } else {
            return null;
        }
    }

    @SuppressWarnings("unused")
    public synchronized void resetAllEmcIds() {
        List<T> updated = new ArrayList<>();

        for (T entity : findAll()) {
            if (entity instanceof EntityWithEmcId) {
                ((EntityWithEmcId) entity).setEmcId(-1);
                updated.add(entity);
            }
        }

        if (!updated.isEmpty()) {
            save(updated);
            // refresh the cache since part of the cache is built on the emcId to id mapping.
            refreshCache();
        }

    }


    @Override
    public <S extends T> S saveNoSequence(S entity) {
        return getRepository().saveNoSequence(entity);
    }


    protected void processUpdate(DataUpdate<T> dataUpdate) {
        switch (dataUpdate.operation) {
            case CREATE:
            case UPDATE:
                T receivedData = dataUpdate.data;
                Optional<T> localDataOptional = getRepository().findOne(getId(receivedData));

                if (localDataOptional.isPresent()) {
                    T localData = localDataOptional.get();
                    // we have this entity, is the receivedData a later version of the existing data
                    int compareResult = compareLatest(localData, receivedData);
                    if (compareResult < 0) {
                        haSave(receivedData);
                    } else if (compareResult == 0) {
                        // only update if this is the stand-by in slave mode
                        if (!haServer.getStatus().isActiveServer()) {
                            haSave(receivedData);
                        }
                    }
                } else {
                    // don't have this data so can go ahead and add it.
                    haSave(receivedData);
                }

                break;
            case DELETE:
                haDelete(dataUpdate.data);

                break;
            case DELETE_ALL:
                haDeleteAll();
                break;
        }
    }

    /**
     * Method to save a HA update received from a peer server.
     *
     * @param update
     */
    protected void haSave(T update) {
        log.debug(String.format("Saving received update [%s] of entity type [%s] to the repository", update, getType()));
        // save to the repository not to the service method otherwise will recreate the peer update
        // also don't increment the version as we have received this update with the correct version.
        getRepository().saveNoSequence(update, false);
        getRepository().alignSequence();
    }

    protected void haDeleteAll() {
        getRepository().deleteAll();
    }

    protected void haDelete(T update) {
        // deleteById from the repository not the service otherwise will recreate a peer deleteById (infinite loop!)
        getRepository().delete(update);
    }


    /**
     * Created by chriswhiteley on 25/04/2017.
     */
    enum CUD {
        CREATE, UPDATE, DELETE, DELETE_ALL
    }


    static class DataUpdate<T> implements Serializable {
        private final T data;
        private final CUD operation;
        private final Class<T> type;

        public DataUpdate(T data, CUD operation, Class<T> type) {
            this.type = type;
            this.data = data;
            this.operation = operation;
        }

        public DataUpdate(CUD operation, Class<T> type) {
            this(null, operation, type);
        }

        public T getData() {
            return data;
        }

        public CUD getOperation() {
            return operation;
        }

        public Class<T> getType() {
            return type;
        }

        @Override
        public String toString() {
            return "DataUpdate{" +
                    "data=" + data +
                    ", operation=" + operation +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof DataUpdate)) return false;

            DataUpdate<?> that = (DataUpdate<?>) o;

            return data.equals(that.data);
        }

        @Override
        public int hashCode() {
            return data.hashCode();
        }
    }


    private class QueueHandler implements Runnable {

        IQueue<DataUpdate<T>> updateQueue;

        private QueueHandler(IQueue<DataUpdate<T>> updateQueue) {
            this.updateQueue = updateQueue;
        }

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    DataUpdate<T> dataUpdate = updateQueue.take();

                    synchronized (this) {
                        processUpdate(dataUpdate);
                    }
                } catch (InterruptedException ie) {
                    log.info(String.format("HA Queue handler thread for entity type [%s] interrupted, exiting thread", getType()));
                    Thread.currentThread().interrupt();
                } catch (IllegalStateException is) {
                    log.info(String.format("HA Queue handler thread for entity type [%s] received IllegalStateException, exiting thread", getType()), is);
                    Thread.currentThread().interrupt();
                } catch (Throwable t) {
                    log.info(String.format("HA Queue handler thread for entity type [%s] received exception, continuing... ", getType()), t);
                }
            }
        }
    }

}
