package net.rebasoft.data;

import java.io.Serializable;
import java.util.Collection;

/**
 * Repository interface with standard deleteById methods.
 *
 * @param <T> The data Type (Entity).
 * @param <ID> The Id (Primary key) type.
 */
public interface RepositoryD <T,ID extends Serializable> extends RepositoryDbyID<T, ID> {


    /**
     * Deletes the entity with the given id.
     *
     * @param id
     */
    void deleteById(ID id);


    /**
     * Deletes a given entity.
     *
     * @param entity
     * @return the deleted entity
     */
    <S extends T> S delete(T entity);

    /**
     * Deletes the given entities.
     *
     * @param entities
     * @return the deleted entities.
     */
     Collection<? extends T> delete(Iterable<? extends T> entities);

    /**
     * Deletes all entities managed by the repository.
     */
    void deleteAll();

}
