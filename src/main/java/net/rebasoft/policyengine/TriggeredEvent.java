package net.rebasoft.policyengine;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;
import java.util.Map;


@AllArgsConstructor
@Getter
public class TriggeredEvent implements Serializable {
    static final long serialVersionUID = 1L;
    private int triggertype;
    private Map<String, Object> data;
}
