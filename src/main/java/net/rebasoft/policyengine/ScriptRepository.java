package net.rebasoft.policyengine;

import javax.script.CompiledScript;
import java.util.Collection;
import java.util.UUID;


public interface ScriptRepository {
    Collection<UUID> getUUIDsForTriggerType(int triggerType);
    CompiledScript getScript(UUID uuid);
    String getName(UUID uuid);
}
