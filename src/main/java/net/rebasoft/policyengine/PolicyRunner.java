package net.rebasoft.policyengine;

import java.util.Map;

interface PolicyRunner {
    void runPolicies(int triggerTypeId, Map<String, Object> data);
}
