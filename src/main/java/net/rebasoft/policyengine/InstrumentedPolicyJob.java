package net.rebasoft.policyengine;

import com.codahale.metrics.Timer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

import java.util.Map;
import java.util.UUID;

@Getter(AccessLevel.PACKAGE)
@Setter(AccessLevel.PACKAGE)
class InstrumentedPolicyJob extends PolicyJob {
    private Timer.Context submitContext;
    private Timer.Context runContext;

    InstrumentedPolicyJob(int triggerType, Map<String, Object> data, UUID policyUUID) {
        super(triggerType, data, policyUUID);
    }

}
