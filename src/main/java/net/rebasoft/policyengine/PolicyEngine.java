package net.rebasoft.policyengine;

import com.codahale.metrics.MetricRegistry;
import com.hazelcast.core.IMap;
import net.rebasoft.data.ha.HAServer;
import net.rebasoft.data.ha.ServerListener;
import net.rebasoft.data.ha.ServerStatus;
import net.rebasoft.emc.utils.NetworkUtils;
import net.rebasoft.policy.TriggerType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Bindings;
import javax.script.SimpleBindings;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;


@SuppressWarnings("unchecked")
public class PolicyEngine extends AbstractPolicyEngine<InstrumentedPolicyJob> implements ServerListener {
    private final Logger log = LoggerFactory.getLogger(PolicyEngine.class);

    private IMap<UUID, TriggeredEvent> haMap;

    private PolicyEngine(Builder builder) {
        super(builder.executorService, builder.scriptRepository, builder.instrumentedRunStage);
        if (builder instanceof HaBuilder) {
            haMap = ((HaBuilder) builder).haMap;
        }
    }

    @Override
    protected InstrumentedPolicyJob newPolicyJob(int triggerType, Map<String, Object> data, UUID policyUUID) {
        return new InstrumentedPolicyJob(triggerType, data, policyUUID);
    }

    @Override
    protected Bindings getBindings(PolicyJob job) {
        // return the common Bindings plus specific ones for this policy job...

        // the following stores state into the script engines bindings object.
//        job.getScript().getEngine().put("Greeting", "hello");

        // bindings is associated with a scope:-
        // global scope - visible to all script engines
        // engine scope - visible only to the script engine to which the bindings object is associated.
        // The ScriptEngineManager class creates the initial global bindings. It also provides setBindings() that can
        // be invoked to replace the global bindings. And getBindings() to retrieve the global bindings.
        // Also put() and get() methods to add to the global bindings.
//        new ScriptEngineManager().setBindings();
//        new ScriptEngineManager().put("global greeting", "hello world!");

        // Why would you want to change a scope's global bindings? You probably wouldn't.
//        new ScriptEngineManager().getEngineByExtension("txt");
//        new ScriptEngineManager().getEngineByName("js");

//
//        job.getScript().getEngine().setBindings();


        // Ok so probably want to put the shared stuff i.e. actions, kommon etc in the GLOBAL i.e. new ScriptEngineManager().set()....
        // then the variable stuff just create a new Bindings object and put the variable stuff in it...

        // this will have all key values from the map returned by getData available directly e.g. deviceIp as an integer variable in the java code.

        Bindings bindings = new SimpleBindings(new ConcurrentHashMap<>(job.getData()));
        bindings.put("data", job.getData());

        bindings.put("log", LoggerFactory.getLogger(job.getScriptName()));
        Optional<TriggerType> triggerTypeOptional = TriggerType.getById(job.getTriggerTypeId());
        triggerTypeOptional.ifPresent(triggerType -> bindings.put("triggerType", triggerType));
        // if there is a device (decimal Ip) in the data map then get it as an InetAddress and store this in the bindings with key "deviceAddress" so that policies can use either.
        Integer deviceDecimal = (Integer) job.getData().get("device");
        if (deviceDecimal != null) {
            // provide InetAddress version of the device for use in policies
            try {
                bindings.put("deviceAddress", InetAddress.getByAddress(NetworkUtils.hostAddressToByteArray(deviceDecimal)));
            } catch (UnknownHostException e) {
                log.error("Error converting decimal ip {} to InetAddress", deviceDecimal, e);
            }
        }
        return bindings;
    }


    /**
     * Should only get called for HA Servers (Master or Standby)
     *
     * @param state the new ServerStatus
     */
    @Override
    public void stateChanged(ServerStatus state) {
        // has this just become the active server if so take over the running of any jobs that were on the HA map
        if (state.isActiveHAServer()) {
            haMap.values().forEach(triggeredEvent -> runPolicies(triggeredEvent.getTriggertype(), triggeredEvent.getData()));
        }
    }

    /**
     * Use this Builder class to build a Policy Engine for a Standalone server.
     */
    static public class Builder  {
        private ExecutorService executorService;
        private ScriptRepository scriptRepository;
        FilterRepository filterRepository;
        MetricRegistry registry;
        InstrumentedRunStage instrumentedRunStage;

        public Builder executorService(ExecutorService executorService) {
            this.executorService = executorService;
            return this;
        }

        public Builder scriptRepository(ScriptRepository scriptRepository) {
            this.scriptRepository = scriptRepository;
            return this;
        }

        public Builder filterRepository(FilterRepository filterRepository) {
            this.filterRepository = filterRepository;
            return this;
        }

        public Builder registry(MetricRegistry registry) {
            this.registry = registry;
            return this;
        }

        public Builder instrumentedRunStage(InstrumentedRunStage instrumentedRunStage) {
            this.instrumentedRunStage = instrumentedRunStage;
            return this;
        }

        private void validate() {
            if (executorService == null) throw new NullPointerException("executorService is required (cannot be null)");
            if (scriptRepository == null)
                throw new NullPointerException("scriptRepository is required (cannot be null)");
            if (filterRepository == null)
                throw new NullPointerException("filterRepository is required (cannot be null)");
            if (registry == null) throw new NullPointerException("registry is required (cannot be null)");
        }

        public InstrumentedRunStage getInstrumentedRunStage() {
            return instrumentedRunStage;
        }

        public PolicyEngine build() {
            validate();
            if (instrumentedRunStage == null)
                instrumentedRunStage = new InstrumentedRunStage(new PolicyRunStage(filterRepository), registry);
            return new PolicyEngine(this);
        }

    }

    /**
     * Use this Builder class to build a Policy Engine for a HA Server (Master or Standby)
     */
    static class HaBuilder extends Builder {
        private HAServer server;
        private IMap<UUID, TriggeredEvent> haMap;


        public HaBuilder server(HAServer server) {
            this.server = server;
            return this;
        }

        public HaBuilder haMap(IMap<UUID, TriggeredEvent> haMap) {
            this.haMap = haMap;
            return this;
        }

        private void validate() {
            super.validate();
            if (server == null) throw new NullPointerException("server is required (cannot be null)");
            if (haMap == null) throw new NullPointerException("haMap is required (cannot be null)");
        }

        public PolicyEngine build() {
            super.validate();
            validate();
            if (instrumentedRunStage == null)
                instrumentedRunStage = new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, haMap), registry);
            return new PolicyEngine(this);
        }
    }

}
