package net.rebasoft.policyengine;

import com.hazelcast.core.IMap;
import net.rebasoft.data.ha.HAServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.UUID;


/**
 * HA version of PolicyRunStage
 *
 */
public class HaPolicyRunStage extends PolicyRunStage <PolicyJob> {
    private final Logger log = LoggerFactory.getLogger(HaPolicyRunStage.class);
    private HAServer server;
    private IMap<UUID, TriggeredEvent> runningJobsMap;

    HaPolicyRunStage(FilterRepository repository, HAServer server, IMap<UUID, TriggeredEvent> runningJobsMap) {
        super(repository);
        this.server = server;
        this.runningJobsMap = runningJobsMap;
    }


    @Override
    public boolean onTrigger(int triggerTypeId, Map<String, Object> data) {
        return server.getStatus().isActiveServer() && super.onTrigger(triggerTypeId, data);
    }


    @Override
    public boolean beforeRun(PolicyJob job) {
        boolean willRunPolicy = super.beforeRun(job);

        if (willRunPolicy) {
            // policy is going to be run so put it on the running jobs map...
            runningJobsMap.set(job.getRunUUID(), new TriggeredEvent(job.getTriggerTypeId(), job.getData()));
        }
        return willRunPolicy;
    }

    @Override
    public void onFinishedRun(PolicyJob job) {
        if (!job.isInterrupted() || job.isCancelled()) {
            runningJobsMap.delete(job.getRunUUID());
        }
    }

}
