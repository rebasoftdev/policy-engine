package net.rebasoft.policyengine;

import lombok.*;

import java.util.Collection;
import java.util.HashSet;
import java.util.UUID;

/**
 * Maps a trigger type to the policies it fires.
 */
@ToString()
@Getter
@EqualsAndHashCode(of = {"triggerType"})
public class TriggeredPolicies {
    private final int triggerType;
    private final Collection<UUID> policyUUIDs;

    public TriggeredPolicies(int triggerType) {
        this.triggerType = triggerType;
        this.policyUUIDs = new HashSet<>();
    }

    public void addPolicyUUID(UUID policyUUID) {
        policyUUIDs.add(policyUUID);
    }

    public void removePolicyUUID(UUID policyUUID) {
        policyUUIDs.remove(policyUUID);
    }

}
