package net.rebasoft.policyengine;

import com.codahale.metrics.Counter;
import com.codahale.metrics.Meter;
import com.codahale.metrics.MetricRegistry;
import com.codahale.metrics.Timer;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Used to provide a RunStage with metric timings.
 */
@SuppressWarnings("unchecked")
public class InstrumentedRunStage <J extends PolicyJob> implements RunStage <InstrumentedPolicyJob> {


    private final MetricRegistry registry;
    private final Map<UUID, Timer> policySubmitTimers;
    private final Map<UUID, Timer> policyRunTimers;
    private final Map<Integer, Meter> triggerTypeMeters;
    private final Map<UUID, Counter> policyFailsCounters;
    private final Map<UUID, Counter> policyBlockedRunsCounters;
    private final RunStage<J> delegate;

    public  InstrumentedRunStage(RunStage <J> delegate, MetricRegistry registry) {
        this.registry = registry;
        this.delegate = delegate;
        policySubmitTimers = new ConcurrentHashMap<>();
        policyRunTimers = new ConcurrentHashMap<>();
        triggerTypeMeters = new ConcurrentHashMap<>();
        policyFailsCounters = new ConcurrentHashMap<>();
        policyBlockedRunsCounters = new ConcurrentHashMap<>();
    }


    @Override
    public boolean onTrigger(int triggerTypeId, Map<String, Object> data) {
        Meter triggerTypeMeter = getTriggerTypeMeter(triggerTypeId);
        triggerTypeMeter.mark();
        return delegate.onTrigger(triggerTypeId, data);
    }

    @Override
    public boolean beforeRun(InstrumentedPolicyJob job) {
        boolean willRun = delegate.beforeRun((J) job);
        if (willRun) {
            Timer submitTimer = getPolicySubmitTimer(job.getPolicyUUID());
            job.setSubmitContext(submitTimer.time());
        } else {
            getBlockedRunsCounter(job.getPolicyUUID()).inc();
        }
        return willRun;
    }

    @Override
    public void onRun(InstrumentedPolicyJob job) {
        Timer runTimer = getPolicyRunTimer(job.getPolicyUUID());
        job.setRunContext(runTimer.time());
        delegate.onRun((J) job);
    }

    @Override
    public void onError(InstrumentedPolicyJob job) {
        delegate.onError((J) job);
        getPolicyFailsCounter(job.getPolicyUUID()).inc();
        job.getRunContext().stop();
        job.getSubmitContext().stop();
    }

    @Override
    public void onFinishedRun(InstrumentedPolicyJob job) {
        delegate.onFinishedRun((J) job);
        job.getRunContext().stop();
        job.getSubmitContext().stop();
    }

    @Override
    public void afterTrigger(int triggerTypeId) {
        delegate.afterTrigger(triggerTypeId);
    }


    private Timer getPolicySubmitTimer(UUID policyUUID) {
        Timer timer = policySubmitTimers.computeIfAbsent(policyUUID, p -> registry.timer(MetricRegistry.name(getClass(), "runPlusWaitTime", p.toString())));

        return timer;
    }

    private Timer getPolicyRunTimer(UUID policyUUID) {
        Timer timer = policyRunTimers.computeIfAbsent(policyUUID, p -> registry.timer(MetricRegistry.name(getClass(), "runTime", p.toString())));

        return timer;
    }


    private Counter getPolicyFailsCounter(UUID policyUUID) {
        Counter counter = policyFailsCounters.computeIfAbsent(policyUUID, p -> registry.counter(MetricRegistry.name(getClass(), "failures", p.toString())));

        return counter;
    }


    private Counter getBlockedRunsCounter(UUID policyUUID) {
        Counter counter = policyBlockedRunsCounters.computeIfAbsent(policyUUID, p -> registry.counter(MetricRegistry.name(getClass(), "blockedRun", p.toString())));

        return counter;
    }


    private Meter getTriggerTypeMeter(int triggerType) {
        Meter meter = triggerTypeMeters.computeIfAbsent(triggerType, t -> registry.meter(MetricRegistry.name(getClass(), "triggerType", Integer.toString(t))));

        return meter;
    }


}
