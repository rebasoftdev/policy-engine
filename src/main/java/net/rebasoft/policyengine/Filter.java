package net.rebasoft.policyengine;

interface Filter {
    @SuppressWarnings("unused")
    boolean isMatch(PolicyJob job);
    String getName();
    String getType();
}
