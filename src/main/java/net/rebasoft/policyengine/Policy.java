package net.rebasoft.policyengine;

import lombok.*;

import javax.script.CompiledScript;
import java.io.Serializable;
import java.util.UUID;

/**
 *
 */
@Getter
@Setter
@RequiredArgsConstructor
@ToString(exclude = {"script", "compiledScript"})
@EqualsAndHashCode(of = {"id"})
class Policy implements Serializable {
    static final long serialVersionUID = 1L;
    @NonNull
    private final UUID id;
    @NonNull
    private String name;
    @NonNull
    private String description;
    @NonNull
    private String script;
    private transient CompiledScript compiledScript;

}
