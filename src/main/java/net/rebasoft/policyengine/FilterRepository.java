package net.rebasoft.policyengine;

import java.util.Collection;
import java.util.UUID;


interface FilterRepository {
    Collection<Filter> getAllFiltersByPolicyUuid(UUID PolicyUUID);
}
