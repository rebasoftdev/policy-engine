package net.rebasoft.policyengine;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.script.Bindings;
import javax.script.CompiledScript;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

/**
 * Abstract Policy Engine class that provides ability to run policy scripts for a trigger event and ability to cancel a policy run.
 * It fires abstract methods relating to stages in the processing of a trigger event and running of associated policy scripts
 * that the implementing class can hook in to:-
 * <p>
 * onTrigger(), beforeRun(), onRun(), onError(), onFinishedRun(), afterTrigger().
 */
public abstract class AbstractPolicyEngine<J extends PolicyJob> implements PolicyRunner {
    private final Logger log = LoggerFactory.getLogger(AbstractPolicyEngine.class);

    private final Map<UUID, J> submittedJobs;
    private final ExecutorService executorService;
    private final ScriptRepository repository;
    private final RunStage <J> runStage;

    AbstractPolicyEngine(ExecutorService executorService, ScriptRepository repository, RunStage<J> runStage) {
        this.executorService = executorService;
        this.repository = repository;
        this.submittedJobs = new ConcurrentHashMap<>();
        this.runStage = runStage;
    }


    @Override
    public void runPolicies(int triggerTypeId, Map<String, Object> data) {

        if (runStage.onTrigger(triggerTypeId, data)) {

            Collection<UUID> policyUUIDs = repository.getUUIDsForTriggerType(triggerTypeId);

            for (UUID policyUUID : policyUUIDs) {
                J job = newPolicyJob(triggerTypeId, data, policyUUID);

                if (runStage.beforeRun(job)) {
                    job.setScriptName(repository.getName(policyUUID));
                    submittedJobs.put(job.getRunUUID(), job);
                    Future<J> policyJobFuture = executorService.submit(new PolicyCallable(job, repository.getScript(policyUUID), getBindings(job)));
                    job.setRunFuture(policyJobFuture);
                }
            }

            runStage.afterTrigger(triggerTypeId);
        }
    }

    protected abstract J newPolicyJob(int triggerType, Map<String, Object> data, UUID policyUUID);


    /**
     * Cancel running job with this run UUID
     * todo: do some overloaded versions of this for different searches to cancel runs i.e. by triggerType, policyUUID etc.
     *
     * @param runUUID UUID of run to cancel
     */
    public void cancelRun(UUID runUUID) {

        PolicyJob job = submittedJobs.get(runUUID);
        if (job != null) {
            job.setCancelled(true);
            job.getRunFuture().cancel(true);
        }

    }

    private void finishRun(J policyJob) {
        submittedJobs.remove(policyJob.getRunUUID());
        runStage.onFinishedRun(policyJob);
    }


    protected abstract Bindings getBindings(PolicyJob job);


    @AllArgsConstructor(access = AccessLevel.PRIVATE)
    private class PolicyCallable implements Callable<J> {

        private J job;
        CompiledScript script;
        Bindings bindings;

        @Override
        public J call() throws Exception {
            runStage.onRun(job);
            try {
                job.setEvalResult(script.eval(bindings));

            } catch (Throwable t) {
                job.setThrowable(t);
                runStage.onError(job);
            }

            finishRun(job);
            return job;
        }
    }


}
