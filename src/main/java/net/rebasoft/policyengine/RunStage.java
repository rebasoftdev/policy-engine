package net.rebasoft.policyengine;

import java.util.Map;

/**
 * Provides callback methods for the different stages when running a policy.
 */
public interface RunStage <J extends PolicyJob> {

    boolean onTrigger( int triggerTypeId, Map<String,Object> data);

    boolean beforeRun(J job);

    void onRun(J job);

    void onError(J job);

    void onFinishedRun(J job);

    void afterTrigger(int triggerTypeId);
}
