package net.rebasoft.policyengine;

import lombok.AllArgsConstructor;

import java.util.Map;


@AllArgsConstructor
public class PolicyRunStage<J extends PolicyJob> implements RunStage<J> {

    private FilterRepository repository;

    @Override
    public boolean onTrigger(int triggerTypeId, Map<String,Object> data) {
        return true;
    }

    @Override
    public boolean beforeRun(J job) {
        // check all filters on this policy match
        // todo: note want to also add the filter name to the bindings? or put it in the data which will then get added to the bindings (perhaps do this in the filter itself!!!)...
        // not sure want to modify the data put it in the job i.e. List of filters used?
        return repository.getAllFiltersByPolicyUuid(job.getPolicyUUID()).stream().allMatch(filter ->
        {
            boolean match = filter.isMatch(job);
            if (match) {
                // if we get a match store the filter details in the policies data map.
                job.getData().put(filter.getType(), filter.getName());
            }

            return match;
        });
    }

    @Override
    public void onRun(J job) {
        // set the thread name to the run UUID
        Thread.currentThread().setName("PE-" + job.getRunUUID().toString());
    }

    @Override
    public void onError(J job) {
        // probably want to tell RMC that this policy had a run Time error....
    }

    @Override
    public void onFinishedRun(J job) {
    }

    @Override
    public void afterTrigger(int triggerTypeId) {
    }
}
