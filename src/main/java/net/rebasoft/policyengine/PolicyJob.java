package net.rebasoft.policyengine;

import lombok.*;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.Future;


@Getter
@Setter(AccessLevel.PACKAGE)
@ToString (exclude = {"runFuture"})
class PolicyJob {
    private final int triggerTypeId;
    private final Map<String, Object> data;
    private final UUID triggerEventUUID;
    private UUID runUUID;
    private UUID policyUUID;

    private String scriptName;
    private Object evalResult;
    @Getter(AccessLevel.PACKAGE) private Future<? extends PolicyJob> runFuture;
    private Throwable throwable;
    private boolean cancelled = false;

    PolicyJob(int triggerTypeId, Map<String, Object> data, UUID policyUUID) {
        this.triggerTypeId = triggerTypeId;
        this.data = new HashMap<>(data);
        this.triggerEventUUID = UUID.randomUUID();
        this.policyUUID = policyUUID;
        this.runUUID = UUID.randomUUID();
    }

    boolean isInterrupted() {
        return (throwable != null) && (throwable instanceof InterruptedException || throwable.getCause() instanceof InterruptedException);
    }

}
