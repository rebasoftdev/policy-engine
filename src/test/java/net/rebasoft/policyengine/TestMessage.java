package net.rebasoft.policyengine;

import java.io.Serializable;

public class TestMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int testNo;
    private String description;
    private Object expectedResult;
    private boolean success;
    private String detailFailMessage;

    public TestMessage(int testNo, String description) {
        this.testNo = testNo;
        this.description = description;
    }

    public TestMessage(TestMessage testMessage) {
        this.testNo = testMessage.getTestNo();
        this.description = testMessage.getDescription();
        this.expectedResult = testMessage.getExpectedResult();
        this.success = testMessage.isSuccess();
        this.detailFailMessage = testMessage.getDetailFailMessage();
    }

    public int getTestNo() {
        return testNo;
    }

    public void setTestNo(int testNo) {
        this.testNo = testNo;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getDetailFailMessage() {
        return detailFailMessage;
    }

    public void setDetailFailMessage(String detailFailMessage) {
        this.detailFailMessage = detailFailMessage;
    }

    public Object getExpectedResult() {
        return expectedResult;
    }

    public String getExpectedTextResult() {
        return (String) expectedResult;
    }

    public void setExpectedResult(Object expectedResult) {
        this.expectedResult = expectedResult;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof TestMessage)) return false;

        TestMessage that = (TestMessage) o;

        return getTestNo() == that.getTestNo();

    }

    @Override
    public int hashCode() {
        return getTestNo();
    }

    @Override
    public String toString() {
        return "TestResultMessage{" +
                "testNo=" + testNo +
                ", description='" + description + '\'' +
                ", success=" + success +
                ", detailFailMessage='" + detailFailMessage + '\'' +
                '}';
    }
}