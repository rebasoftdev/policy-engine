package net.rebasoft.policyengine;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import javax.script.*;
import java.util.*;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;


@SuppressWarnings("unchecked")
public class AbstractPolicyEngineTest {

    private static ScriptEngine engine = new ScriptEngineManager().getEngineByName("js");


    /**
     * Test that implementation can block event running (for example when we are on the Standby slave server)
     *
     * @throws Exception exception from test
     */
    @Test
    public void runPoliciesBlockRun() throws Exception {
        ScriptRepository scriptRepository = Mockito.mock(ScriptRepository.class);
        RunStage runStage = Mockito.mock(RunStage.class);
        PolicyEngineDummyImpl engine = Mockito.spy(new PolicyEngineDummyImpl(Executors.newFixedThreadPool(1),  scriptRepository, runStage));

        int triggerType = 1000;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();
        doReturn(false).when(runStage).onTrigger(triggerType, data);

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);

        // in this case this should halt the run so we would not expect to see any other methods called...
        verify(runStage, never()).beforeRun(any());
        verify(runStage, never()).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, never()).onFinishedRun(any());
        verify(runStage, never()).afterTrigger(triggerType);
    }

    /**
     * Test that implementation can block policy running due to for example a filter mismatch
     *
     * @throws Exception exception from test
     */
    @Test
    public void runPoliciesBlockRunFilterMismatch() throws Exception {
        ScriptRepository scriptRepository = Mockito.mock(ScriptRepository.class);
        RunStage runStage = Mockito.mock(RunStage.class);
        PolicyEngineDummyImpl engine = Mockito.spy(new PolicyEngineDummyImpl(Executors.newFixedThreadPool(1), scriptRepository, runStage));

        int triggerType = 1000;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        doReturn(true).when(runStage).onTrigger(triggerType, data);
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(ArgumentMatchers.eq(triggerType));
        ArgumentCaptor<PolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(PolicyJob.class);

        // false being returned to stop run of policy because of a filter mismatch
        doReturn(false).when(runStage).beforeRun(jobArgumentCaptor.capture());

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(runStage, times(1)).beforeRun(any());

        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertEquals("Trigger data is as expected", data, jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());
        assertEquals("PolicyUUID is as expected", policyUUIDs.get(0), jobArgumentCaptor.getValue().getPolicyUUID());

        // in this case this should halt the run so we would not expect to see the following methods called...
        verify(runStage, never()).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, never()).onFinishedRun(any());

        // we should see afterTrigger() called once
        verify(runStage, times(1)).afterTrigger(triggerType);
    }


    @Test
    public void runPoliciesSinglePolicyRun() throws Exception {
        ScriptRepository scriptRepository = Mockito.mock(ScriptRepository.class);
        RunStage runStage = Mockito.mock(RunStage.class);
        PolicyEngineDummyImpl engine = Mockito.spy(new PolicyEngineDummyImpl(Executors.newFixedThreadPool(1), scriptRepository, runStage));

        int triggerType = 123;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        doReturn(true).when(runStage).onTrigger(triggerType, data);
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(ArgumentMatchers.eq(triggerType));
        ArgumentCaptor<PolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(PolicyJob.class);
        doReturn(true).when(runStage).beforeRun(jobArgumentCaptor.capture());
        doReturn(getCompiledScript("greeting = 'Hello World';")).when(scriptRepository).getScript(ArgumentMatchers.eq(policyUUIDs.get(0)));
        doReturn(getBindings()).when(engine).getBindings(any());

        engine.runPolicies(triggerType, data);

        // wait for policy to finish
        jobArgumentCaptor.getValue().getRunFuture().get();

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(runStage, times(1)).beforeRun(any());
        verify(scriptRepository, times(1)).getScript(any());
        verify(runStage, times(1)).beforeRun(any());
        verify(engine, times(1)).getBindings(any());
        verify(runStage, times(1)).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, times(1)).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);

        // check the result of the script run
        System.out.println(jobArgumentCaptor.getValue().getEvalResult());
        assertEquals("result of Script run", "Hello World", jobArgumentCaptor.getValue().getEvalResult());
        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertEquals("Trigger data is as expected", data, jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());
        assertEquals("PolicyUUID is as expected", policyUUIDs.get(0), jobArgumentCaptor.getValue().getPolicyUUID());
        assertNull("Expect no exception was generated", jobArgumentCaptor.getValue().getThrowable());

    }


    /**
     * Test a running a policy with a runtime error.
     *
     * @throws Exception exception from test
     */
    @Test
    public void runPoliciesSinglePolicyRuntimeError() throws Exception {
        ScriptRepository scriptRepository = Mockito.mock(ScriptRepository.class);
        RunStage runStage = Mockito.mock(RunStage.class);
        PolicyEngineDummyImpl engine = Mockito.spy(new PolicyEngineDummyImpl(Executors.newFixedThreadPool(1), scriptRepository, runStage));

        int triggerType = 123;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        doReturn(true).when(runStage).onTrigger(triggerType, data);
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(ArgumentMatchers.eq(triggerType));
        ArgumentCaptor<PolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(PolicyJob.class);
        doReturn(true).when(runStage).beforeRun(jobArgumentCaptor.capture());
        // this should generate a run error since the method does not exist
        doReturn(getCompiledScript("window.printme();")).when(scriptRepository).getScript(ArgumentMatchers.eq(policyUUIDs.get(0)));
        doReturn(getBindings()).when(engine).getBindings(any());

        engine.runPolicies(triggerType, data);

        // wait for policy to finish
        jobArgumentCaptor.getValue().getRunFuture().get();

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(runStage, times(1)).beforeRun(any());
        verify(scriptRepository, times(1)).getScript(any());
        verify(runStage, times(1)).beforeRun(any());
        verify(engine, times(1)).getBindings(any());
        verify(runStage, times(1)).onRun(any());
        verify(runStage, times(1)).onError(any());
        verify(runStage, times(1)).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);

        // check the result of the script run
        assertNull("result of Script run should be null as expecting runtime exception", jobArgumentCaptor.getValue().getEvalResult());
        assertNotNull("Expect an exception was generated", jobArgumentCaptor.getValue().getThrowable());
        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertEquals("Trigger data is as expected", data, jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());
        assertEquals("PolicyUUID is as expected", policyUUIDs.get(0), jobArgumentCaptor.getValue().getPolicyUUID());
    }


    String sleepTwoSecondsPolicyText = "load(\"nashorn:mozilla_compat.js\");\n" +
            "var Thread = java.lang.Thread;\n" +
            "  Thread.sleep(2000);";

    /**
     * Test we can cancel a running policy
     *
     * @throws Exception exception from test
     */
    @Test
    public void cancelRun() throws Exception {
        ScriptRepository scriptRepository = Mockito.mock(ScriptRepository.class);
        RunStage runStage = Mockito.mock(RunStage.class);
        PolicyEngineDummyImpl engine = Mockito.spy(new PolicyEngineDummyImpl(Executors.newFixedThreadPool(1), scriptRepository, runStage));

        int triggerType = 123;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        doReturn(true).when(runStage).onTrigger(triggerType, data);
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(ArgumentMatchers.eq(triggerType));
        ArgumentCaptor<PolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(PolicyJob.class);
        doReturn(true).when(runStage).beforeRun(jobArgumentCaptor.capture());
        doReturn(getCompiledScript(sleepTwoSecondsPolicyText)).when(scriptRepository).getScript(ArgumentMatchers.eq(policyUUIDs.get(0)));
        doReturn(getBindings()).when(engine).getBindings(any());

        engine.runPolicies(triggerType, data);

        // sleep for a bit then cancel the job....
        Thread.sleep(500);
        engine.cancelRun(jobArgumentCaptor.getValue().getRunUUID());

        // wait for policy to finish
        try {
            jobArgumentCaptor.getValue().getRunFuture().get();
            fail("Job was not cancelled");
        } catch (CancellationException ce) {
            System.out.println ("Job was cancelled!");
        }

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(scriptRepository, times(1)).getScript(any());
        verify(runStage, times(1)).beforeRun(any());
        verify(engine, times(1)).getBindings(any());
        verify(runStage, times(1)).onRun(any());
        verify(runStage, times(1)).onError(any());
        verify(runStage, times(1)).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);

        // check the result of the script run
        assertNull("result of Script run should be null as expecting runtime exception", jobArgumentCaptor.getValue().getEvalResult());
        assertNotNull("Expect an exception was generated", jobArgumentCaptor.getValue().getThrowable());
        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertEquals("Trigger data is as expected", data, jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());
        assertEquals("PolicyUUID is as expected", policyUUIDs.get(0), jobArgumentCaptor.getValue().getPolicyUUID());
    }


    private CompiledScript getCompiledScript(String scriptText) throws ScriptException {
        return ((Compilable) engine).compile(scriptText);
    }

    private Bindings getBindings() {
        return engine.getBindings(ScriptContext.ENGINE_SCOPE);
    }


    /**
     * Dummy implementation of AbstractPolicyEngine
     */
    private static class PolicyEngineDummyImpl extends AbstractPolicyEngine <PolicyJob> {


        PolicyEngineDummyImpl(ExecutorService executorService, ScriptRepository scriptRepository, RunStage runStage) {
            super(executorService, scriptRepository, runStage);
        }

        @Override
        protected PolicyJob newPolicyJob(int triggerType, Map<String, Object> data, UUID policyUUID) {
            return new PolicyJob(triggerType, data, policyUUID);
        }

        @Override
        protected Bindings getBindings(PolicyJob job) {
            return null;
        }

    }

}