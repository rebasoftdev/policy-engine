package net.rebasoft.policyengine;

import com.codahale.metrics.InstrumentedExecutorService;
import com.codahale.metrics.MetricRegistry;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;
import net.rebasoft.data.ha.HAServer;
import net.rebasoft.data.ha.ServerStatus;
import net.rebasoft.policy.TriggerType;
import org.mockito.ArgumentCaptor;
import org.mockito.Mockito;
import org.slf4j.LoggerFactory;

import javax.script.Compilable;
import javax.script.CompiledScript;
import javax.script.ScriptException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.*;


@SuppressWarnings("unchecked")
class StandByProcess implements Runnable {

    private final org.slf4j.Logger log = LoggerFactory.getLogger(StandByProcess.class);

    private final HAServer server;

    private final FilterRepository filterRepository;

    private final ScriptRepository scriptRepository;

    private final HazelcastInstance instance;
    private boolean inMultiNodeCluster = false;

    private final ThreadPoolExecutor executor;
    private final PolicyEngine engine;
    private final InstrumentedRunStage runStage;

    StandByProcess() {
        instance = Hazelcast.newHazelcastInstance();
        IMap<UUID, TriggeredEvent> runningEventsMap = instance.getMap(PolicyEngineTest.RUNNING_EVENTS_MAP_NAME);
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(11);
        MetricRegistry metricRegistry = new MetricRegistry();
        ExecutorService executorService = new InstrumentedExecutorService(executor, metricRegistry, "PE-StandBy-ExecutorService");
        server = Mockito.mock(HAServer.class);
        filterRepository = Mockito.mock(FilterRepository.class);
        scriptRepository = Mockito.mock(ScriptRepository.class);

        runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(executorService)
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        engine = builder.build();
    }


    private TestMessage runTestStart() {
        TestMessage message = new TestMessage(PolicyEngineTest.START_TEST_NO, "StandByProcess started.");
        message.setSuccess(inMultiNodeCluster);
        return message;
    }

    private TestMessage runTestFinish() {
        TestMessage message = new TestMessage(PolicyEngineTest.END_TEST_NO, "Finish test");
        message.setSuccess(true);
        return message;
    }

    private void resetMocks() {
        reset(server, scriptRepository, filterRepository, runStage);
    }


    private TestMessage runTest1(TestMessage testMessage) {
        resetMocks();
        StringBuilder actualResults = new StringBuilder();

        try {
            // generate 50 events these should be ignored.

            int triggerType = TriggerType.NewMACNotification;
            ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

            data.put("greeting", "Hello there :-)");
            data.put("from", "Chris");
            data.put("device", 1368692020);

            // simulate server is a STANDBY_WITH_MASTER so is not active and should not be running any policies.
            doReturn(ServerStatus.STANDBY_WITH_MASTER).when(server).getStatus();

            // simulate that there is a policy for this trigger type
            List<UUID> policyUUIDs = new ArrayList<>();
            policyUUIDs.add(UUID.randomUUID());
            doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

            // return a match filter for this policy, so it should run.
            List<Filter> filtersMatch = new ArrayList<>();
            filtersMatch.add(new MatchFilter());
            doReturn(filtersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

            // return script for this policy that calls an action method that should be in the global bindings of the script scriptEngine.

            Mockito.doReturn(getCompiledScript(
                    "        log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                            "        log.warn(greeting);\n" +
                            "        log.warn(from + ' about to call action.doAction1 (int)...');\n" +
                            "        actions.doAction1(device);\n" +
                            "        log.warn(from + ' about to call action.doAction1 (InetAddress)...');\n" +
                            "        actions.doAction1(deviceAddress);\n" +
                            "        log.warn('finished policy');")).when(scriptRepository).getScript(policyUUIDs.get(0));


            // return a name for this policy script
            doReturn("DoActionPolicy").when(scriptRepository).getName(policyUUIDs.get(0));

            // simulate 50 events to fire the policy
            for (int eventNo = 0; eventNo < 50; eventNo++) {
                engine.runPolicies(triggerType, data);
            }


            ArgumentCaptor<Integer> triggerTypeArgumentCaptor = ArgumentCaptor.forClass(Integer.class);

            verify(runStage, times(50)).onTrigger(triggerTypeArgumentCaptor.capture(), any());

            assertEquals("TriggerType is as expected", triggerType, triggerTypeArgumentCaptor.getValue().intValue());

            verify(runStage, never()).beforeRun(any());
            verify(runStage, never()).onRun(any());
            verify(runStage, never()).onError(any());
            verify(runStage, never()).onFinishedRun(any());
            verify(runStage, never()).afterTrigger(triggerType);

        } catch (Throwable t) {
            log.error("Test failed", t);
            actualResults.append(t.getMessage());
        }

        return createResultMessage(testMessage, actualResults);
    }


    /**
     * In test 2 we are going to switch to an active Standby server (STANDBY_NO_MASTER).
     *
     * @param testMessage test message received from master (PolicyEngineTest)
     * @return resultMessage
     */
    private TestMessage runTest2(TestMessage testMessage) {
        resetMocks();
        StringBuilder actualResults = new StringBuilder();

        try {
            // simulate server is a STANDBY_NO_MASTER so is capable of running policies.
            doReturn(ServerStatus.STANDBY_NO_MASTER).when(server).getStatus();

            ArgumentCaptor<InstrumentedPolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(InstrumentedPolicyJob.class);

            // simulate that there is a policy for this trigger type
            List<UUID> policyUUIDs = new ArrayList<>();
            policyUUIDs.add(UUID.randomUUID());
            doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(TriggerType.NewMACNotification);

            // return a match filter for this policy, so it should run.
            List<Filter> filtersMatch = new ArrayList<>();
            filtersMatch.add(new MatchFilter());
            doReturn(filtersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

            // return script for this policy that re-runs the same policy we were doing on Master.
            doReturn(getCompiledScript(
                    "        log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                            "        log.warn(greeting);\n" +
                            "        log.warn(from + ' about to call action.doSleep (' + sleepValue + ')...');\n" +
                            "        actions.doSleep(sleepValue);\n" +
                            "        log.warn('finished policy');")).when(scriptRepository).getScript(policyUUIDs.get(0));

            // return a name for this policy script
            doReturn("StandBy - DoSleepActionPolicy").when(scriptRepository).getName(policyUUIDs.get(0));

            // simulate server state change to STANDBY_NO_MASTER, this should transfer running jobs from runningEventsMap and re-run them
            engine.stateChanged(ServerStatus.STANDBY_NO_MASTER);

            // wait for all jobs to finish.....
            waitForJobsToFinish();

            int triggerType = TriggerType.NewMACNotification;
            verify(runStage, times(50)).onTrigger(eq(triggerType), any());
            verify(runStage, times(50)).beforeRun(jobArgumentCaptor.capture());

            assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
            assertNotNull("Trigger data is not null", jobArgumentCaptor.getValue().getData());
            assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
            assertEquals("Policy UUID is as expected", policyUUIDs.get(0), jobArgumentCaptor.getValue().getPolicyUUID());
            assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());


            verify(runStage, times(50)).onRun(any());
            verify(runStage, never()).onError(any());
            verify(runStage, times(50)).onFinishedRun(any());
            verify(runStage, times(50)).afterTrigger(triggerType);

        } catch (Throwable t) {
            log.error("Test failed", t);
            actualResults.append(t.getMessage());
        }

        return createResultMessage(testMessage, actualResults);
    }


    /**
     * In test 3 we are still an active Standby server (STANDBY_NO_MASTER).
     * Now generate 50 events these should be processed.
     *
     * @param testMessage test message received from master (PolicyEngineTest)
     * @return resultMessage
     */
    private TestMessage runTest3(TestMessage testMessage) {
        resetMocks();
        StringBuilder actualResults = new StringBuilder();

        try {
            // simulate server is a STANDBY_NO_MASTER so is capable of running policies.
            doReturn(ServerStatus.STANDBY_NO_MASTER).when(server).getStatus();

            int triggerType = TriggerType.AllARP;

            ArgumentCaptor<InstrumentedPolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(InstrumentedPolicyJob.class);

            // simulate that there is a policy for this trigger type
            List<UUID> policyUUIDs = new ArrayList<>();
            policyUUIDs.add(UUID.randomUUID());
            doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

            // return a match filter for this policy, so it should run.
            List<Filter> filtersMatch = new ArrayList<>();
            filtersMatch.add(new MatchFilter());
            doReturn(filtersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

            // return script for this policy that calls an action method that should be in the global bindings of the script scriptEngine.
            doReturn(getCompiledScript(
                    "        log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                            "        log.warn(greeting);\n" +
                            "        log.warn(from + ' about to call action.doAction1 (int)...');\n" +
                            "        actions.doAction1(device);\n" +
                            "        log.warn(from + ' about to call action.doAction1 (InetAddress)...');\n" +
                            "        actions.doAction1(deviceAddress);\n" +
                            "        log.warn('finished policy');")).when(scriptRepository).getScript(policyUUIDs.get(0));

            // return a name for this policy script
            doReturn("StandBy - DoActionPolicy").when(scriptRepository).getName(policyUUIDs.get(0));

            PolicyEngineTest.generate50DeviceTriggerEvents(TriggerType.AllARP, engine);

            // wait for all jobs to finish.....
            waitForJobsToFinish();

            verify(runStage, times(50)).onTrigger(eq(triggerType), any());
            verify(runStage, times(50)).beforeRun(jobArgumentCaptor.capture());

            assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
            assertNotNull("Trigger data is not null", jobArgumentCaptor.getValue().getData());
            assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
            assertEquals("Policy UUID is as expected", policyUUIDs.get(0), jobArgumentCaptor.getValue().getPolicyUUID());
            assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());

            verify(runStage, times(50)).beforeRun(any());
            verify(runStage, times(50)).onRun(any());
            verify(runStage, never()).onError(any());
            verify(runStage, times(50)).onFinishedRun(any());
            verify(runStage, times(50)).afterTrigger(triggerType);

        } catch (Throwable t) {
            log.error("Test failed", t);
            actualResults.append(t.getMessage());
        }

        return createResultMessage(testMessage, actualResults);
    }

    private void waitForJobsToFinish() throws InterruptedException {
        while (executor.getActiveCount() > 0) {
            Thread.sleep(100);
        }
    }


    private TestMessage createResultMessage(TestMessage testMessage, StringBuilder actualResults) {
        TestMessage resultMessage = new TestMessage(testMessage);
        resultMessage.setSuccess(actualResults.length() == 0);
        resultMessage.setDetailFailMessage(actualResults.toString());
        return resultMessage;
    }

    @Override
    public void run() {
        IQueue<TestMessage> testNoQueue = instance.getQueue(PolicyEngineTest.TESTNO_QUEUE_NAME);
        IQueue<TestMessage> testResultsQueue = instance.getQueue(PolicyEngineTest.TEST_RESULTS_QUEUE_NAME);
        log.debug("************   StandByProcess STARTED *****************");

        boolean finished = false;

        // wait for up to 10 seconds or until we have 2 members in the cluster
        for (int i = 0; i < 10; i++) {
            inMultiNodeCluster = instance.getCluster().getMembers().size() > 1;
            if (inMultiNodeCluster) break;
            try {
                TimeUnit.SECONDS.sleep(1);
            } catch (InterruptedException e) {
                finished = true;
                break;
            }
        }

        if (!inMultiNodeCluster) {
            log.error("Not able to establish Hazelcast cluster of more than one node.");
            finished = true;
        }

        while (!finished) {
            TestMessage testMessage;
            try {
                testMessage = testNoQueue.take();
                log.debug(String.format("received test %s from testNoQueue", testMessage));
            } catch (InterruptedException e) {
                break;
            }

            switch (testMessage.getTestNo()) {
                case PolicyEngineTest.START_TEST_NO:
                    try {
                        TestMessage resultMessage = runTestStart();
                        log.debug("putting result message on testResultQueue " + resultMessage);
                        testResultsQueue.put(resultMessage);
                    } catch (InterruptedException e) {
                        finished = true;
                    }
                    break;
                case 1:
                    try {
                        TestMessage resultMessage = runTest1(testMessage);
                        log.debug("putting result message on testResultQueue " + resultMessage);
                        testResultsQueue.put(resultMessage);
                    } catch (InterruptedException e) {
                        finished = true;
                    }
                    break;

                case 2:
                    try {
                        TestMessage resultMessage = runTest2(testMessage);
                        log.debug("putting result message on testResultQueue " + resultMessage);
                        testResultsQueue.put(resultMessage);
                    } catch (InterruptedException e) {
                        finished = true;
                    }
                    break;

                case 3:
                    try {
                        TestMessage resultMessage = runTest3(testMessage);
                        log.debug("putting result message on testResultQueue " + resultMessage);
                        testResultsQueue.put(resultMessage);
                    } catch (InterruptedException e) {
                        finished = true;
                    }
                    break;


                case PolicyEngineTest.END_TEST_NO:
                    try {
                        TestMessage resultMessage = runTestFinish();
                        log.debug("putting result message on testResultQueue " + resultMessage);
                        testResultsQueue.put(resultMessage);
                    } catch (InterruptedException ignored) {
                    }
                    finished = true;
                    break;
            }
        }

        log.debug("Finished!");


    }


    private CompiledScript getCompiledScript(String scriptText) throws ScriptException {
        return ((Compilable) PolicyEngineTest.scriptEngine).compile(scriptText);
    }


}
