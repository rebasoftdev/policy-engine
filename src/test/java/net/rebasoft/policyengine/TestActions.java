package net.rebasoft.policyengine;

import net.rebasoft.emc.utils.NetworkUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;


public class TestActions {
    private static final Logger log = LoggerFactory.getLogger(TestActions.class);

    public void doAction1(InetAddress ipAddress) {

        log.debug("doAction1, received ip  InetAddress {}... about to do some work on it", ipAddress);
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            log.warn("doAction1, was interrupted before completing action.");
            return;
        }
        log.info("doAction1, completed action on ip {} ", ipAddress);

    }

    public void doAction1(int ipAsDecimal) {
        try {
            log.debug("doAction1, received ip decimal {}", ipAsDecimal);
            doAction1(InetAddress.getByAddress(NetworkUtils.hostAddressToByteArray(ipAsDecimal)));
        } catch (UnknownHostException e) {
            log.error("Error converting decimal ip {} to InetAddress", ipAsDecimal);
        }
    }

    public void doSleep(int milliseconds) throws InterruptedException {
        log.debug("doSleep, about to sleep {} milliseconds", milliseconds);
        Thread.sleep(milliseconds);
        log.info("doSleep finished");
  }

}
