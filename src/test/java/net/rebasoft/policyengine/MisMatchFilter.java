package net.rebasoft.policyengine;

class MisMatchFilter implements Filter {

    @Override
    public boolean isMatch(PolicyJob job) {
        return false;
    }

    public String getName() {
        return "MisMatchFilter";
    }

    public String getType() {
        return "TestFilter";
    }

}
