package net.rebasoft.policyengine;

import com.codahale.metrics.InstrumentedExecutorService;
import com.codahale.metrics.MetricRegistry;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import com.hazelcast.core.IMap;
import com.hazelcast.core.IQueue;
import net.rebasoft.data.ha.HAServer;
import net.rebasoft.data.ha.ServerStatus;
import net.rebasoft.policy.TriggerType;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import javax.script.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

@SuppressWarnings("unchecked")
@RunWith(MockitoJUnitRunner.class)
public class PolicyEngineTest {
    private final org.slf4j.Logger log = LoggerFactory.getLogger(PolicyEngineTest.class);
    static final String RUNNING_EVENTS_MAP_NAME = "RunningEventsMap";
    static final String TESTNO_QUEUE_NAME = "testno_queue";
    static final int START_TEST_NO = 0;
    static final int END_TEST_NO = 99;
    static final String TEST_RESULTS_QUEUE_NAME = "test_results_queue";


    static ScriptEngine scriptEngine = getScriptEngine();


    @Mock private IMap<UUID, TriggeredEvent> runningEventsMap;

    @Mock private HAServer server;

    @Mock private FilterRepository filterRepository;

    @Mock private ScriptRepository scriptRepository;

    private static final MetricRegistry metricRegistry = new MetricRegistry();

    private ThreadPoolExecutor executor;
    private ExecutorService executorService;

    static void generate50DeviceTriggerEvents(int triggerType, PolicyEngine engine) {
        int deviceIpDecimal = 1368692020;

        for (int i = 0; i < 50; i++) {
            ConcurrentHashMap<String, Object> data2 = new ConcurrentHashMap<>();
            data2.put("greeting", "Hello there :-) " + i);
            data2.put("from", "Chris " + i);
            data2.put("device", deviceIpDecimal++);
            engine.runPolicies(triggerType, data2);
        }
    }


    @Before
    public void beforeEachTest() {
        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(11);
        executorService = new InstrumentedExecutorService(executor, metricRegistry, "PE-ExecutorService");
    }

    @After
    public void afterEachTest() {
        if (!executorService.isShutdown()) executorService.shutdown();
    }


    @SuppressWarnings("EmptyMethod")
    @BeforeClass
    public static void onStartTesting() {
//        ConsoleReporter reporter = ConsoleReporter.forRegistry(metricRegistry)
//                .convertRatesTo(TimeUnit.SECONDS)
//                .convertDurationsTo(TimeUnit.MILLISECONDS)
//                .build();
//        reporter.start(1, TimeUnit.SECONDS);
    }

    @SuppressWarnings("EmptyMethod")
    @AfterClass
    public static void onFinishTesting() {
//        try {
//            Thread.sleep(5000);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
//        reporter.stop();
    }


    /**
     * Test that implementation can block event running (for example when we are on the Standby slave server)
     *
     * @throws Exception exception from test
     */
    @Test
    public void runPoliciesBlockRunWhenStandbyInSlaveMode() throws Exception {

        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.AllARP;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        data.put("greeting", "Hello there :-)");

        // simulate server is a STANDBY connected to a MASTER (i.e. is in "Slave" mode) so shouldn't run policies.
        doReturn(ServerStatus.STANDBY_WITH_MASTER).when(server).getStatus();

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);

        // in this case this should halt the run so we would not expect to see any other methods called...
        verify(runStage, never()).beforeRun(any());
        verify(runStage, never()).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, never()).onFinishedRun(any());
        verify(runStage, never()).afterTrigger(triggerType);

    }

    /**
     * Test condition when a trigger event happens but there are no policies set up to be run for this trigger type
     *
     * @throws Exception exception from test
     */
    @Test
    public void runPoliciesNoPoliciesToRun() throws Exception {

        // set up a non HA Run Stage to simulate a stand-alone server.
        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new PolicyRunStage(filterRepository), metricRegistry));

        // this time we are a Stand Alone server so build for stand-alone (no HA Map and server)
        PolicyEngine.Builder builder =
                new PolicyEngine.Builder()
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.RadiusAccounting;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        data.put("deviceIp", "81.148.149.52");
        data.put("macAddress", "00-15-E9-2B-99-3C");


        // simulate that there are no policies for this trigger type
        List<UUID> policyUUIDs = new ArrayList<>();
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(eq(triggerType), any());

        verify(runStage, never()).beforeRun(any());
        verify(runStage, never()).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, never()).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);

    }


    /**
     * Test that implementation can block policy running due to a filter mismatch
     *
     * @throws Exception exception from test
     */
    @Test
    public void runPoliciesBlockRunWhenFilterMismatch() throws Exception {

        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.OneOff;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        data.put("greeting", "Hello there :-)");

        ArgumentCaptor<InstrumentedPolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(InstrumentedPolicyJob.class);


        // simulate server is a MASTER_WITH_STANDBY so is capable of running policies.
        doReturn(ServerStatus.MASTER_WITH_STANDBY).when(server).getStatus();

        // simulate that there are policies for this trigger type
        Collection<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

        // but return a mismatch filter for this policy, so shouldn't run.
        Collection<Filter> noFiltersMatch = new ArrayList<>();
        noFiltersMatch.add(new MisMatchFilter());
        doReturn(noFiltersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(runStage, times(1)).beforeRun(jobArgumentCaptor.capture());

        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertEquals("Trigger data is as expected", data, jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertEquals("Policy UUID is as expected", policyUUIDs.toArray()[0], jobArgumentCaptor.getValue().getPolicyUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());

        // beforeRun should be called and afterTrigger but the policy should not be run so onRun, onError, onFinishedRun should not be called

        verify(runStage, never()).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, never()).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);
    }


    @Test
    public void runPoliciesSinglePolicyRun() throws Exception {

        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.OneMinuteTrigger;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        data.put("greeting", "Hello there :-)");
        data.put("from", "Chris");

        ArgumentCaptor<InstrumentedPolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(InstrumentedPolicyJob.class);


        // simulate server is a MASTER_NO_STANDBY so is capable of running policies.
        doReturn(ServerStatus.MASTER_NO_STANDBY).when(server).getStatus();

        // simulate that there are policies for this trigger type
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

        // return a match filter for this policy, so it should run.
        List<Filter> filtersMatch = new ArrayList<>();
        filtersMatch.add(new MatchFilter());
        doReturn(filtersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

        // return script for this policy
        doReturn(getCompiledScript(" log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                "        log.warn(greeting);\n" +
                "        log.warn(from);\n" +
                "        log.info('test filter was ' + testFilter);")).when(scriptRepository).getScript(policyUUIDs.get(0));

        // return a name for this policy script
        doReturn("GreetingPolicy").when(scriptRepository).getName(policyUUIDs.get(0));

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(runStage, times(1)).beforeRun(jobArgumentCaptor.capture());

        // wait for policy to finish...
        jobArgumentCaptor.getValue().getRunFuture().get();

        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertNotNull("Trigger data is as expected", jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertEquals("Policy UUID is as expected", policyUUIDs.toArray()[0], jobArgumentCaptor.getValue().getPolicyUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());

        verify(runStage, times(1)).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, times(1)).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);


//        for (int i = 0; i < 1000 ; i++) {
//            ConcurrentHashMap<String, Object> data2 = new ConcurrentHashMap<>();
//            data2.put("greeting", "Hello there :-) " + i);
//            data2.put("from", "Chris " + i);
//            scriptEngine.runPolicies(triggerType, data2);
//        }

    }


    // todo: do a test where more than one policy is run
    @Test
    public void runPoliciesMultiplePolicyRun() throws Exception {

        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.DailyTrigger;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        data.put("greeting", "Hello there :-)");
        data.put("from", "Chris");

        ArgumentCaptor<InstrumentedPolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(InstrumentedPolicyJob.class);

        // simulate server is a MASTER_NO_STANDBY so is capable of running policies.
        doReturn(ServerStatus.MASTER_NO_STANDBY).when(server).getStatus();

        // simulate that there are 3 policies for this trigger type
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        policyUUIDs.add(UUID.randomUUID());
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

        // return no filters for any policy, so all should run.
        List<Filter> noFilters = new ArrayList<>();
        doReturn(noFilters).when(filterRepository).getAllFiltersByPolicyUuid(any());

        // return script for each policy
        doReturn(getCompiledScript(" log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                "        log.info('1. This is policy ' + log.getName());\n" +
                "        log.debug ('one')\n" +
                "        log.warn(greeting);\n" +
                "        log.warn(from);\n")).when(scriptRepository).getScript(policyUUIDs.get(0));
        doReturn(getCompiledScript(" log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                "        log.info('2. This is policy ' + log.getName());\n" +
                "        log.info ('two')\n" +
                "        log.warn(greeting);\n" +
                "        log.warn(from);\n")).when(scriptRepository).getScript(policyUUIDs.get(1));
        doReturn(getCompiledScript(" log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                "        log.info('3. This is policy ' + log.getName());\n" +
                "        log.warn ('three')\n" +
                "        log.warn(greeting);\n" +
                "        log.warn(from);\n")).when(scriptRepository).getScript(policyUUIDs.get(2));


        // return a name for each policy script
        doReturn("Daily Policy 1").when(scriptRepository).getName(policyUUIDs.get(0));
        doReturn("Daily Policy 2").when(scriptRepository).getName(policyUUIDs.get(1));
        doReturn("Daily Policy 3").when(scriptRepository).getName(policyUUIDs.get(2));

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);

        // wait for policies to finish...
        waitForJobsToFinish();

        verify(runStage, times(3)).beforeRun(jobArgumentCaptor.capture());

        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertNotNull("Trigger data is as expected", jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertNotNull("Policy UUID not null", jobArgumentCaptor.getValue().getPolicyUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());

        verify(runStage, times(3)).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, times(3)).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);

    }

    // do a test to see that we can call some actions method that is stored in the global bindings....
    @Test
    public void runPoliciesSinglePolicyCallingActionMethod() throws Exception {

        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.NewMACNotification;
        ConcurrentHashMap<String, Object> data = new ConcurrentHashMap<>();

        data.put("greeting", "Hello there :-)");
        data.put("from", "Chris");
        data.put("device", 1368692020);

        ArgumentCaptor<InstrumentedPolicyJob> jobArgumentCaptor = ArgumentCaptor.forClass(InstrumentedPolicyJob.class);

        // simulate server is a STANDBY_NO_MASTER so is capable of running policies.
        doReturn(ServerStatus.STANDBY_NO_MASTER).when(server).getStatus();

        // simulate that there are policies for this trigger type
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

        // return a match filter for this policy, so it should run.
        List<Filter> filtersMatch = new ArrayList<>();
        filtersMatch.add(new MatchFilter());
        doReturn(filtersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

        // return script for this policy that calls an action method that should be in the global bindings of the script scriptEngine.
        doReturn(getCompiledScript(
                "        log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                        "        log.warn(greeting);\n" +
                        "        log.warn(from + ' about to call action.doAction1 (int)...');\n" +
                        "        actions.doAction1(device);\n" +
                        "        log.warn(from + ' about to call action.doAction1 (InetAddress)...');\n" +
                        "        actions.doAction1(deviceAddress);\n" +
                        "        log.warn('finished policy, emailing ' + emailTo);")).when(scriptRepository).getScript(policyUUIDs.get(0));

        // return a name for this policy script
        doReturn("DoActionPolicy").when(scriptRepository).getName(policyUUIDs.get(0));

        engine.runPolicies(triggerType, data);

        verify(runStage, times(1)).onTrigger(triggerType, data);
        verify(runStage, times(1)).beforeRun(jobArgumentCaptor.capture());

        // wait for policy to finish...
        jobArgumentCaptor.getValue().getRunFuture().get();

        assertEquals("TriggerType is as expected", triggerType, jobArgumentCaptor.getValue().getTriggerTypeId());
        assertNotNull("Trigger data is not null", jobArgumentCaptor.getValue().getData());
        assertNotNull("Trigger event UUID is not null", jobArgumentCaptor.getValue().getTriggerEventUUID());
        assertEquals("Policy UUID is as expected", policyUUIDs.toArray()[0], jobArgumentCaptor.getValue().getPolicyUUID());
        assertNotNull("Run UUID is not null", jobArgumentCaptor.getValue().getRunUUID());

        verify(runStage, times(1)).onRun(any());
        verify(runStage, never()).onError(any());
        verify(runStage, times(1)).onFinishedRun(any());
        verify(runStage, times(1)).afterTrigger(triggerType);

        generate50DeviceTriggerEvents(triggerType, engine);

        waitForJobsToFinish();

    }


    /**
     * HA tests.
     *
     * @throws Exception exception from test
     */
    @Test
    public void runHaTests() throws Exception {
        // simulate standby in a thread
        ExecutorService standByExecutor = Executors.newSingleThreadExecutor();
        Future standbyProcessFuture = standByExecutor.submit(new StandByProcess());

        HazelcastInstance instance = Hazelcast.newHazelcastInstance();

        IMap<UUID, TriggeredEvent> runningEventsMap = instance.getMap(RUNNING_EVENTS_MAP_NAME);
        IQueue<TestMessage> testRunQueue = instance.getQueue(TESTNO_QUEUE_NAME);
        IQueue<TestMessage> testResultsQueue = instance.getQueue(TEST_RESULTS_QUEUE_NAME);

        boolean commEstablished = false;
        // check that we can communicate with the Stand By process (i.e. we are in a 2 node Hazelcast cluster).
        testRunQueue.put(new TestMessage(START_TEST_NO, "Startup"));
        try {
            TestMessage testResultMessage = testResultsQueue.poll(10, TimeUnit.SECONDS);
            commEstablished = testResultMessage != null && testResultMessage.isSuccess();
            if (!commEstablished) {
                log.error("Unable to setup testing environment: ");
            }
        } catch (InterruptedException ie) {
            log.error("Unable to establish communication with Standby Process, possibly Hazelcast cluster of 2 nodes not established.");
        }

        Assume.assumeTrue(commEstablished);


        // tell Stand-By to generate 50 events these should be ignored...
        testRunQueue.put(new TestMessage(1, "Test 1 - Generate 50 trigger events"));
        TestMessage results1 = testResultsQueue.take();
        assertTrue("If success these were ignored/not processed.", results1.isSuccess());


        // run some policies as master....
        InstrumentedRunStage runStage = Mockito.spy(new InstrumentedRunStage(new HaPolicyRunStage(filterRepository, server, runningEventsMap), metricRegistry));

        PolicyEngine.Builder builder =
                new PolicyEngine.HaBuilder()
                        .haMap(runningEventsMap)
                        .server(server)
                        .executorService(getExecutor())
                        .filterRepository(filterRepository)
                        .scriptRepository(scriptRepository)
                        .instrumentedRunStage(runStage)
                        .registry(metricRegistry);

        PolicyEngine engine = builder.build();

        int triggerType = TriggerType.NewMACNotification;

        // simulate server is a MASTER_WITH_STANDBY so is capable of running policies.
        doReturn(ServerStatus.MASTER_WITH_STANDBY).when(server).getStatus();

        // simulate that there is one policy for this trigger type
        List<UUID> policyUUIDs = new ArrayList<>();
        policyUUIDs.add(UUID.randomUUID());
        doReturn(policyUUIDs).when(scriptRepository).getUUIDsForTriggerType(triggerType);

        // return a match filter for this policy, so it should run.
        List<Filter> filtersMatch = new ArrayList<>();
        filtersMatch.add(new MatchFilter());
        doReturn(filtersMatch).when(filterRepository).getAllFiltersByPolicyUuid(any());

        doReturn(getCompiledScript(
                "        log.info ('This policy was fired by a [' + triggerType.getName() + '] trigger');\n" +
                        "        log.warn(greeting);\n" +
                        "        log.warn(from + ' about to call action.doSleep (' + sleepValue + ')...');\n" +
                        "        actions.doSleep(sleepValue);\n" +
                        "        log.warn('finished policy');")).when(scriptRepository).getScript(policyUUIDs.get(0));

        // return a name for this policy script
        doReturn("Master DoSleepActionPolicy").when(scriptRepository).getName(policyUUIDs.get(0));

        // run up 50 events/policies that should get transferred to Standby When master goes down...

        for (int i = 0; i < 50; i++) {
            ConcurrentHashMap<String, Object> data2 = new ConcurrentHashMap<>();
            data2.put("greeting", "Hello there :-) " + i);
            data2.put("from", "Chris " + i);
            data2.put("sleepValue", 500);
            engine.runPolicies(triggerType, data2);
        }

        // now kill Master
        executorService.shutdownNow();

        // tell Standby to take over...
        testRunQueue.put(new TestMessage(2, "Test 2 - make Standby the active server i.e. STANDBY_NO_MASTER"));
        TestMessage results2 = testResultsQueue.take();
        assertTrue("Switched to Standby and 50 events taken over and processed.", results2.isSuccess());
        waitForJobsToFinish();
        // tell StandBy to generate 50 events these should be processed as is now an active Standby (STANDBY_NO_MASTER)...
        testRunQueue.put(new TestMessage(3, "Test 3 - generate 50 events"));
        TestMessage results3 = testResultsQueue.take();
        assertTrue("50 events generated and processed.", results3.isSuccess());

        // stop standby once complete....
        standbyProcessFuture.cancel(true);
        executor.shutdown();
    }

    private void waitForJobsToFinish() throws InterruptedException {
        while (executor.getActiveCount() > 0) {
            Thread.sleep(100);
        }
    }


    private ExecutorService getExecutor() {
        return executorService;
    }

    private static ScriptEngine getScriptEngine() {
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();

        // set up global bindings...
        scriptEngineManager.put("actions", new TestActions());
        scriptEngineManager.put("emailTo", "chris.whiteley@rebasoft.com");


        scriptEngine = scriptEngineManager.getEngineByName("js");

        return scriptEngine;

    }


    private CompiledScript getCompiledScript(String scriptText) throws ScriptException {
        return ((Compilable) scriptEngine).compile(scriptText);
    }


}