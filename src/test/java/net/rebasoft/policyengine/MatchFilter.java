package net.rebasoft.policyengine;

class MatchFilter implements Filter {

    @Override
    public boolean isMatch(PolicyJob job) {
        return true;
    }

    public String getName() {
        return "MatchFilter";
    }

    public String getType() {
        return "testFilter";
    }

}
